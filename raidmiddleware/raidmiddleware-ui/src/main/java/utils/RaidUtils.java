package utils;

import cloud.content.Raid1Content;
import cloud.content.Raid5Fragment;
import cloud.content.RaidContent;
import cloud.interfaces.ICloudBaseHandler;

public class RaidUtils {

	public static enum HandlerID {
		AMAZON ("Amazon S3"), 
		DROPBOX ("Dropbox"), 
		BOX ("Box");
		
		private String name;
		private HandlerID(String name) {this.name = name;}
		public String getName() {
			return name;
		}
	}

	public static RaidContent getRaidContentFromCloud(byte[] fileContent, ICloudBaseHandler handler) {
		RaidContent raidContent = null;
		byte magicNo = fileContent[0];		
		
		if (magicNo == RaidContent.RAID1_MAGIC_NUMBER)
			raidContent = new Raid1Content(handler);
		else if (magicNo == RaidContent.RAID5_MAGIC_NUMBER) {
			raidContent = new Raid5Fragment(handler);
		} else {
			// magic number not set
			raidContent = new RaidContent(handler);
		}
		
		raidContent.setBytes(fileContent);
		
		return raidContent;
	}

}
