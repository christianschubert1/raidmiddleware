package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Raid5Utils {
	private final static int BLOCK_SIZE = 2; // in bytes
	private final static int PARITY_START_POS = 2; // disk no.
	
	public static byte[] getContentFromRaid5Files(byte[] disk1, byte[] disk2, byte[] disk3) {
		List<byte[]> disks = Arrays.asList(disk1,disk2,disk3);
		// all available disks (which are not null)
		List<byte[]> disksAvailable = new ArrayList<byte[]>(disks);
		disksAvailable.removeAll(Collections.singleton(null));
		
		// raid5 criterion: at least 2 disks required to obtain data
		if (disksAvailable.size() < 2)
			return null;
		
		int diskSize = disksAvailable.get(0).length;
		byte[] content = new byte[diskSize * 2];
		int contentIndex = 0;
		int parityPos = PARITY_START_POS;
		
		
		for (int i=0; i < (diskSize / BLOCK_SIZE); i++) {
			List<Integer> dataPos = getDataPositions(parityPos);
			byte[] parity = disks.get(parityPos);
			byte[] block1 = disks.get(dataPos.get(0));
			byte[] block2 = disks.get(dataPos.get(1));
			readBlock(content, disks.get(dataPos.get(0)), i, contentIndex, parity, block2);
			contentIndex += BLOCK_SIZE;
			readBlock(content, disks.get(dataPos.get(1)), i, contentIndex, parity, block1);
			contentIndex += BLOCK_SIZE;
			// change parity position
			parityPos = nextParityPosition(parityPos);
		}
		
		return content;
	}
	
	private static void readBlock(byte[] out, byte[] block, int blockPosition, int outOffset, byte[] parity, byte[] otherBlock) {
		for (int i=0; i < BLOCK_SIZE; i++) {
			byte data = 0;
			int blockIndex = blockPosition*BLOCK_SIZE+i;
			if (block == null) {
				// compute missing data from parity and other block
				data = (byte) (parity[blockIndex] ^ otherBlock[blockIndex]);
			} else {
				data = block[blockIndex];
			}
			out[outOffset+i] = data;
		}
	}
	
	private static List<Integer> getDataPositions(int parityPos) {
		List<Integer> dataPos = new ArrayList<Integer>(Arrays.asList(0,1,2));
		dataPos.remove(parityPos);
		return dataPos;
	}
	
	public static int nextParityPosition(int currentParityPos) {
		// forward layout
		return (currentParityPos + 1) % 3;
	}
	
	//returns 3 byte-arrays as raid 5 data for disk1, disk2 and disk3
	public static byte[][] getRaid5Fragments(byte[] content) {
				// new content size must be divisible by block_size * 2
				int newContentSize = getNextDivisible(content.length, BLOCK_SIZE*2);
				int raidContentSize = newContentSize / 2;
				
				byte[][] fragments = new byte[3][raidContentSize];		
				
				int fragments_index = 0;
				int parityPos = PARITY_START_POS;
				
//				System.out.println("content size: " + content.length);
//				System.out.println("new content size: " + newContentSize);
//				System.out.println("raidContentSize: " + raidContentSize);

				int maxContentIndex = content.length - 1;
				
				for (int i=0; i < (newContentSize / 2); i+=2) {
					List<Integer> dataPos = getDataPositions(parityPos);
					
					int startBlockIndex = i*BLOCK_SIZE;
					int endBlockIndex = startBlockIndex + BLOCK_SIZE;
					byte[] block1 = Arrays.copyOfRange(content, startBlockIndex, endBlockIndex);
					
					// next block					
					startBlockIndex += BLOCK_SIZE;
					endBlockIndex += BLOCK_SIZE;
					
					byte[] block2 = new byte[BLOCK_SIZE];
					if (startBlockIndex <= maxContentIndex)
						block2 = Arrays.copyOfRange(content, startBlockIndex, endBlockIndex);
				
					// compute parity block
					byte[] parity = computeParityBlock(block1, block2);
					
					// write to fragments
					for (int j = 0; j < BLOCK_SIZE; j++) {
						fragments[dataPos.get(0)][fragments_index] = block1[j];
						fragments[dataPos.get(1)][fragments_index] = block2[j];
						fragments[parityPos][fragments_index] = parity[j];
						fragments_index++;
					}
					
				  // set next parity position
					parityPos = nextParityPosition(parityPos);
				}
				
				return fragments;
	}
	
	private static int getNextDivisible(int number, int divider) {
		int rem = number % divider;
		
		if (rem != 0)
			number += divider - rem;
		
		return number;
	}
	
	public static byte[] computeParityBlock(byte[] block1, byte[] block2) {
		byte[] parityBlock = new byte[block1.length];
		
		for (int i=0; i < block1.length; i++)
			parityBlock[i] = (byte) (block1[i] ^ block2[i]);
		
		return parityBlock;
	}
	
	public static byte[] recoverMissingFragment(byte[] fragment1, byte[] fragment2) {
		// same as parity block computation
		return computeParityBlock(fragment1, fragment2);
	}
}
