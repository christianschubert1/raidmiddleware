package vaadinui;

import javax.servlet.annotation.WebServlet;

import main.RaidController;

import org.apache.log4j.BasicConfigurator;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;

@Theme("mytheme")
@PreserveOnRefresh
@Widgetset("at.ac.tuwien.aic.topic2.raidmiddleware.RaidMiddlewareWidgetset")
public class RaidMiddlewareUI extends UI {
  private static final long serialVersionUID = 1L;
  
  private final RaidController raidController;
  private Navigator navigator;
  private TextArea logTextArea;
  
  public TextArea getLogTextArea() {
    return logTextArea;
  }

  public RaidMiddlewareUI() {
    logTextArea = new TextArea();
    logTextArea.setImmediate(true);
    logTextArea.setSizeFull();
    MyCustomAppender.logTextArea = logTextArea;
    
    BasicConfigurator.configure();
    raidController = new RaidController();
  }

  @Override
  protected void init(VaadinRequest vaadinRequest) {
    navigator = new Navigator(this, this);
    navigator.addView("", new FileBrowserView());
  }

  public RaidController getRaidController() {
    return raidController;
  }

  @WebServlet(urlPatterns = "/*", name = "RaidMiddlewareServlet", asyncSupported = true)
  @VaadinServletConfiguration(ui = RaidMiddlewareUI.class, productionMode = false)
  public static class RaidMiddlewareServlet extends VaadinServlet {
    private static final long serialVersionUID = 1L;
  }
}
