package vaadinui;

import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;

public class RaidNotificationUtil {

  public static void showSuccess(String msg) {
    Notification success = new Notification("ERFOLGREICH", msg);   
    success.setDelayMsec(5000);
    success.setHtmlContentAllowed(true);
    success.setStyleName("humanized bar success small closable");
    success.setPosition(Position.TOP_CENTER);
    success.show(Page.getCurrent());
  } 
  
  public static void showError(String msg) {
    Notification error = new Notification("ERROR", msg);   
    error.setDelayMsec(5000);
    error.setHtmlContentAllowed(true);
    error.setStyleName("humanized bar error small closable");   
    error.setPosition(Position.TOP_CENTER);
    error.show(Page.getCurrent());
  } 
  
  public static void showWarning(String msg) {
    Notification error = new Notification("ACHTUNG", msg);   
    error.setDelayMsec(5000);
    error.setHtmlContentAllowed(true);
    error.setStyleName("humanized bar warning small closable");   
    error.setPosition(Position.TOP_CENTER);
    error.show(Page.getCurrent());
  } 
  
  public static void showInfo(String msg) {
    Notification error = new Notification("INFORMATION", msg);   
    error.setDelayMsec(5000);
    error.setHtmlContentAllowed(true);
    error.setStyleName("humanized bar system small closable");
    error.setPosition(Position.TOP_CENTER);
    error.show(Page.getCurrent());
  } 
}
