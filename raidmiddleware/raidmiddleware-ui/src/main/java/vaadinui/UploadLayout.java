package vaadinui;

import main.RaidController;

import org.vaadin.easyuploads.UploadField;
import org.vaadin.easyuploads.UploadField.FieldType;

import cloud.entry.EntryWrapper;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.ValoTheme;

public class UploadLayout extends HorizontalLayout {
  private static final long serialVersionUID = 1L;
  
  private RaidController controller;
  private String filename;
  private Button uploadNow;
  private ComboBox comboRaidMode;
  
  private static final String COMBOBOX_RAID_1 = "RAID-1";
  private static final String COMBOBOX_RAID_5 = "RAID-5";


  public UploadLayout(final FileBrowserView rootView) {    
//    setWidth("100%");
    setSpacing(true);
    setVisible(false);
    controller = ((RaidMiddlewareUI)UI.getCurrent()).getRaidController();
    uploadNow = new Button("OK");
    comboRaidMode = new ComboBox();
    comboRaidMode.setInvalidAllowed(false);
    comboRaidMode.setNullSelectionAllowed(false);
    comboRaidMode.addItem(COMBOBOX_RAID_5);
    comboRaidMode.addItem(COMBOBOX_RAID_1);
    comboRaidMode.setValue(COMBOBOX_RAID_5);
    
    final UploadField uploadField5 = new UploadField() {
      private static final long serialVersionUID = 1L;
      
      @Override
      protected String getDeleteCaption() {
        return "Cancel";
      }
      
      protected void buildDefaulLayout() {
        super.buildDefaulLayout();
        getRootLayout().addComponent(uploadNow);
        getRootLayout().addComponent(comboRaidMode);
    }


      @Override
      protected String getDisplayDetails() {
          filename = getLastFileName();
          String mimeType = getLastMimeType();
          long filesize = getLastFileSize();
          String info = "Filename: " + filename + "</br>" 
              + "Size: " + filesize + "</br>"
              + "Type: " + mimeType + "</br>";
          return info;     
      }
    };
    uploadField5.addStyleName("upload-button-style");
    uploadField5.setImmediate(true);
    uploadField5.setFieldType(FieldType.BYTE_ARRAY);
    uploadField5.setCaption(null);
    uploadField5.setDescription(null);

//    uploadNow.addStyleName(ValoTheme.BUTTON_PRIMARY);
    
    uploadField5.addValueChangeListener(new ValueChangeListener() {
      private static final long serialVersionUID = 1L;
      @Override
      public void valueChange(ValueChangeEvent event) {
        uploadNow.setEnabled(true);
      }
    });

    uploadNow.addClickListener(new ClickListener() { 
      private static final long serialVersionUID = 1L;

      @Override
      public void buttonClick(ClickEvent event) {
        if(uploadField5.getValue() == null) {
          Notification.show("Attention", "No file to upload selected", Type.WARNING_MESSAGE);
          return;
        }
        int raidMode = comboRaidMode.getValue() == COMBOBOX_RAID_5 ? EntryWrapper.RAID_5_MODE : EntryWrapper.RAID_1_MODE;
        byte[] bytes = (byte[]) uploadField5.getValue();
        controller.uploadFile(filename, bytes, raidMode);
        setVisible(false);
        rootView.ls();
      }
    });

    addComponent(uploadField5);
  }
}
