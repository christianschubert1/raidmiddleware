package vaadinui;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.vaadin.dialogs.ConfirmDialog;

import main.RaidController;
import cloud.entry.EntryWrapper;
import cloud.interfaces.ICloudBaseHandler;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.MouseEventDetails.MouseButton;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.Window;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.themes.ValoTheme;

public class FileBrowserView extends VerticalLayout implements View {
  private static final long serialVersionUID = 1L;
  
  private enum TableHeader { Name, Type, Size, RaidLevel, Clouds, Versions};
  
  private Label labelPath;
  private Button buttonBack;
  private RaidController controller;
  private TreeTable entryTable;
  private MenuBar actionMenuBar;
  private UploadLayout uploadlayout;
  private FolderCreateLayout folderCreateLayout;

  public FileBrowserView() {
    setMargin(true);
    setSpacing(true);
    controller = ((RaidMiddlewareUI)UI.getCurrent()).getRaidController();
    
    if(!controller.getSqlConn().testDB()) {
      Label infoLabel = new Label("Achtung: Keine Verbindung zur SQL Datenbank!");
      infoLabel.addStyleName(ValoTheme.LABEL_H1);
      addComponent(infoLabel);
      UI.getCurrent().close();
      return;
    }
    
    addComponent(buildHeaderInfo());    
    addComponent(buildNavBar());    
    
    uploadlayout = new UploadLayout(this);
    addComponent(uploadlayout);
    
    folderCreateLayout = new FolderCreateLayout(this);
    addComponent(folderCreateLayout);

    Component tableComp = buildUI();
    addComponent(tableComp);
    
    Component loggingComp = buildLoggingArea();
    addComponent(loggingComp);
 
    setExpandRatio(tableComp, 1.0f);
    setExpandRatio(loggingComp, 0.0f);
    cd("");
    ls();
  }
  
  public void ls() {
    entryTable.removeAllItems();
    IndexedContainer container = (IndexedContainer) entryTable.getContainerDataSource();
    Map<String, List<EntryWrapper>> map = controller.ls("", true);

    for (Entry<String, List<EntryWrapper>> entrySet : map.entrySet()) {
      List<Integer> versions = new ArrayList<>();
      String owners = "";
      Long filesize = 0L;
      String filePath = entrySet.getKey();
      EntryWrapper ew = entrySet.getValue().get(0);
      boolean isFile = ew.isFile();
      int raidMode = ew.getRaidMode();

      if(isFile) {
        try {
          if(!controller.getSqlConn().fileExists(controller.getCloudPath(filePath))) {
            byte[] file = controller.downloadFile(filePath);
            controller.getSqlConn().addOrUpdateFile(controller.getCloudPath(filePath), file, raidMode);
          }
	      	
          versions = controller.getSqlConn().getAllVersions(controller.getCloudPath(filePath)); 
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      
      Item item = container.addItem(filePath + "root");
      item.getItemProperty("Name").setValue(buildNameButton(filePath, !entrySet.getValue().get(0).isFile()));
      item.getItemProperty("Type").setValue(entrySet.getValue().get(0).isFile() ? "File" : "Folder");
      item.getItemProperty("RaidLevel").setValue(entrySet.getValue().get(0).getRaidMode() == EntryWrapper.RAID_5_MODE ? "RAID5" : "RAID1");

//      addItemToTable(entrySet.getValue().get(0), container.addItem(filePath + "root"));
      
      for (EntryWrapper entry : entrySet.getValue()) {
        owners += entry.getOwner() + " ";
        filesize += entry.getSize(); 
        addItemToTable(entry, container.addItem(filePath + entry.getOwner() + "child"), versions);
        entryTable.setParent(filePath + entry.getOwner() + "child", filePath + "root");
      }
      
      
      item.getItemProperty("Size").setValue(!entrySet.getValue().get(0).isFile() ? "-" : filesize.toString());
      item.getItemProperty("Clouds").setValue(owners);
      if(isFile)
      	item.getItemProperty("Versions").setValue(addVersionToComboBox(versions, true, filePath));
    
      entryTable.setPageLength(entryTable.size() + 1);
    }
  }
  
  private ComboBox addVersionToComboBox(final List<Integer> versions, boolean enabled, final String path) {
    final ComboBox versionBox = new ComboBox();
    versionBox.addStyleName(ValoTheme.COMBOBOX_TINY);
    versionBox.setTextInputAllowed(false);
    versionBox.setNullSelectionAllowed(false);
    versionBox.setEnabled(enabled);
    
    versionBox.addItems(versions);
    if(!versions.isEmpty())
      versionBox.select(versions.get(versions.size()-1));

    versionBox.addValueChangeListener(new ValueChangeListener() {    
      private static final long serialVersionUID = 1L;

      @Override
      public void valueChange(final ValueChangeEvent event) {
        ConfirmDialog.show(UI.getCurrent(), "Restore to other Version", "Are you sure you want to restore to version " 
            + event.getProperty().getValue(), "Yes", "No", new ConfirmDialog.Listener() {          
          private static final long serialVersionUID = 1L;

          @Override
          public void onClose(ConfirmDialog arg0) {
            if(arg0.isConfirmed())
              controller.restoreToVersion(path, (int) event.getProperty().getValue());
            ls();
          }
        });
      }
    });
    return versionBox;
  }
  
  private void addItemToTable(EntryWrapper entry, Item item, List<Integer> versions) {    
    item.getItemProperty("Name").setValue(buildNameButton(entry.getFilename(), !entry.isFile()));
    item.getItemProperty("Type").setValue(entry.isFile() ? "File" : "Folder");
    item.getItemProperty("Size").setValue(!entry.isFile() ? "-" : entry.getSize() == null ? "0" : entry.getSize().toString());
    item.getItemProperty("RaidLevel").setValue(entry.getRaidMode() == EntryWrapper.RAID_5_MODE ? "RAID5" : "RAID1");
    item.getItemProperty("Clouds").setValue(entry.getOwner());
    
    if(!versions.isEmpty())
      item.getItemProperty("Versions").setValue(addVersionToComboBox(versions, false, entry.getFilename()));
  }
  
  private void cd(String foldername) {
    controller.cd(foldername);
    String path = "";
    if(!controller.getCurrentPath().toString().isEmpty())
      path = controller.getCurrentPath().toString().replaceAll("\\\\", "/") + "/";
   
    labelPath.setValue("Navigation: home/" + path);
    buttonBack.setEnabled(!labelPath.getValue().equals("Navigation: home/"));   
  }
  
  private Component buildNameButton(String name, boolean clickable) {    
    if(clickable) {
      Button button = new Button(name);
      button.addStyleName(ValoTheme.BUTTON_BORDERLESS);
      button.addStyleName(ValoTheme.BUTTON_LINK);
      
      button.addClickListener(new ClickListener() {      
        private static final long serialVersionUID = 1L;

        @Override
        public void buttonClick(ClickEvent event) {
         
         Button source = (Button) event.getSource();
         cd(source.getCaption());
         ls();
        }
      });
      return button;
    }
    else {
      return new Label(name);
    }
  }
  
  private Component buildHeaderInfo() {
    VerticalLayout verticalLayout = new VerticalLayout();
    verticalLayout.setMargin(false);
    verticalLayout.setSpacing(false);
    verticalLayout.setWidth("100%");
      
    Label infoLabel = new Label("Cloud Raid Controller");
    infoLabel.setSizeFull();
    infoLabel.addStyleName(ValoTheme.LABEL_H2);
    infoLabel.addStyleName(ValoTheme.LABEL_BOLD);
    infoLabel.addStyleName(ValoTheme.LABEL_COLORED);
    infoLabel.addStyleName(ValoTheme.LABEL_NO_MARGIN);
    verticalLayout.addComponent(infoLabel);
    
    String mode = controller.getMODE() > 1 ? "RAID5-Mode: ON, RECOVERY-Mode: ON" : "RAID5-Mode: OFF, RECOVERY-Mode: OFF";
    Label modeLabel = new Label(mode);
    modeLabel.addStyleName(ValoTheme.LABEL_H4);  
    verticalLayout.addComponent(modeLabel);
    
    String cloudsAvailable = "Available Clouds: ";
    for (ICloudBaseHandler h : controller.getHandlerList()) {
      cloudsAvailable += h.getID() + ", ";
    }
    
    Label cloudsLabel = new Label(cloudsAvailable.substring(0, cloudsAvailable.length()-2));
    cloudsLabel.addStyleName(ValoTheme.LABEL_NO_MARGIN);
    cloudsLabel.addStyleName(ValoTheme.LABEL_H4);
    verticalLayout.addComponent(cloudsLabel);
    
    verticalLayout.addComponent(new Label("<hr/>", ContentMode.HTML));
    return verticalLayout;
  }
  
  private Component buildNavBar() {
    HorizontalLayout horizontalLayout = new HorizontalLayout();
    horizontalLayout.setSpacing(true);
    horizontalLayout.setWidth("100%");
    
    labelPath = new Label();
    labelPath.addStyleName(ValoTheme.LABEL_BOLD);
    labelPath.addStyleName(ValoTheme.LABEL_LARGE);
    labelPath.setWidth("100%");
    labelPath.addStyleName(ValoTheme.LABEL_COLORED);
    
    buttonBack = new Button("Back", FontAwesome.REPLY);
    buttonBack.addStyleName(ValoTheme.BUTTON_SMALL);
    buttonBack.addClickListener(new ClickListener() {
      private static final long serialVersionUID = 1L;

      @Override
      public void buttonClick(ClickEvent event) {
        cd("..");
        ls();
      }
    });
    
    Button buttonRefresh = new Button("", FontAwesome.REFRESH);
    buttonRefresh.addStyleName(ValoTheme.BUTTON_SMALL);
    buttonRefresh.addClickListener(new ClickListener() {     
      private static final long serialVersionUID = 1L;

      @Override
      public void buttonClick(ClickEvent event) {
        ls();
      }
    });
    
    actionMenuBar = new MenuBar();
    actionMenuBar.addStyleName(ValoTheme.MENUBAR_SMALL);
    
    MenuItem fileItem = actionMenuBar.addItem("File", FontAwesome.FILE_O, null);
    
    fileItem.addItem("Upload", FontAwesome.CLOUD_UPLOAD, new Command() {      
      private static final long serialVersionUID = 1L;
      @Override
      public void menuSelected(MenuItem selectedItem) {
        uploadlayout.setVisible(!uploadlayout.isVisible());
      }
    });
    
    MenuItem folderItem = actionMenuBar.addItem("Folder", FontAwesome.FOLDER_O, null);
    
    folderItem.addItem("New", FontAwesome.FOLDER_OPEN_O, new Command() {      
      private static final long serialVersionUID = 1L;
      @Override
      public void menuSelected(MenuItem selectedItem) {
        folderCreateLayout.setVisible(!folderCreateLayout.isVisible());
      }
    });
    
    horizontalLayout.addComponent(actionMenuBar);
    horizontalLayout.addComponent(labelPath);
    horizontalLayout.addComponent(buttonBack);
    horizontalLayout.addComponent(buttonRefresh);
    horizontalLayout.setExpandRatio(labelPath, 1.0f);
    return horizontalLayout;
  }
	
  private Component buildUI() {   
    VerticalLayout tableLayout = new VerticalLayout();
    tableLayout.setSizeFull();
    tableLayout.addStyleName(ValoTheme.LAYOUT_CARD);

    entryTable = new TreeTable();
    entryTable.addStyleName("button-table-style");
    entryTable.setSelectable(true);
    entryTable.setSizeFull();
    entryTable.addStyleName(ValoTheme.TABLE_BORDERLESS);
    entryTable.addStyleName(ValoTheme.TABLE_COMPACT);
    entryTable.addStyleName(ValoTheme.TABLE_NO_HORIZONTAL_LINES);
    
    entryTable.addItemClickListener(new ItemClickListener() {
      private static final long serialVersionUID = 1L;

      @Override
      public void itemClick(ItemClickEvent event) {
        for (Window win : UI.getCurrent().getWindows()) {
          win.close();
        }

        if (event.getButton()== MouseButton.RIGHT) {
          if(event.getItemId().toString().contains("child"))
            return;
  
          Component component = (Component) event.getItem().getItemProperty("Name").getValue();
          final String name = component instanceof Button ? ((Button) component).getCaption() : ((Label) component).getValue();
          
          entryTable.select(event.getItemId());
          
          final Window window = new Window();
          window.setDraggable(false);
          window.addStyleName(ValoTheme.WINDOW_TOP_TOOLBAR);
          window.setResizable(false);
          window.setClosable(true);
          window.setPositionX(event.getClientX());
          window.setPositionY(event.getClientY());
          
          VerticalLayout layout = new VerticalLayout();
          layout.setMargin(false);
          layout.setSpacing(false);
          
          Button delButton = new Button("Delete", new ClickListener() {
            private static final long serialVersionUID = 1L;
            @Override
            public void buttonClick(ClickEvent event) {
              controller.delete(name);
              window.close();
              ls();
            }
          });
          delButton.setIcon(FontAwesome.TRASH_O);
          delButton.addStyleName(ValoTheme.BUTTON_LINK);
          delButton.addStyleName(ValoTheme.BUTTON_TINY);

          Button downButton = new Button("Download");
          downButton.setIcon(FontAwesome.CLOUD_DOWNLOAD);
          downButton.addStyleName(ValoTheme.BUTTON_LINK);
          downButton.addStyleName(ValoTheme.BUTTON_TINY);
          
          final FileDownloader fileDownloader = new FileDownloader(getMyResource(name));
          fileDownloader.extend(downButton);
          
          downButton.addClickListener(new ClickListener() {
            private static final long serialVersionUID = 1L;

            @Override
            public void buttonClick(ClickEvent event) {
                ls();
            }
          });
          downButton.setImmediate(true);
          
          layout.addComponent(delButton);
          
          String folder = (String) event.getItem().getItemProperty("Type").getValue();
          if(!folder.equals("Folder"))
            layout.addComponent(downButton);
          
          window.setContent(layout);
          UI.getCurrent().addWindow(window);
          window.focus();
        }
      }
    });

//    IndexedContainer container = new IndexedContainer();    
    for (TableHeader header : TableHeader.values()) {
      if(header == TableHeader.Name || header == TableHeader.Versions) 
        entryTable.addContainerProperty(header.name(), Component.class, null);
      else
        entryTable.addContainerProperty(header.name(), String.class, null);
    }

    entryTable.setHeight(400, Unit.PIXELS);
    
//    entryTable.setContainerDataSource(container);
//    entryTable.setPageLength(entryTable.size() + 1);
    tableLayout.addComponent(entryTable);
    return tableLayout;
  }
  
  public StreamResource getMyResource(final String filename) {
    return new StreamResource(new StreamResource.StreamSource() {           
      private static final long serialVersionUID = 1L;
        @Override
        public InputStream getStream() {
          return new ByteArrayInputStream(controller.downloadFile(filename));
        }
    }, filename);
  }
  
  private Component buildLoggingArea() {   
    VerticalLayout loggingLayout = new VerticalLayout();
    loggingLayout.setWidth("100%");
    loggingLayout.addStyleName(ValoTheme.LAYOUT_CARD);
    loggingLayout.addComponent(((RaidMiddlewareUI)UI.getCurrent()).getLogTextArea());
    return loggingLayout;
  }

	@Override
  public void enter(ViewChangeEvent event) {
  }
}
