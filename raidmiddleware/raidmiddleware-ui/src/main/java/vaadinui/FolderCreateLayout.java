package vaadinui;

import main.RaidController;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class FolderCreateLayout extends HorizontalLayout {
  private static final long serialVersionUID = 1L;
  
  private RaidController controller;
  private TextField txtFoldername;
  private Button createNow;

  public FolderCreateLayout(final FileBrowserView rootView) {    
//    setWidth("100%");
    setSpacing(true);
    setVisible(false);
    controller = ((RaidMiddlewareUI)UI.getCurrent()).getRaidController();
    
    Label label = new Label("New Folder: ");
    
    txtFoldername = new TextField();
    txtFoldername.setImmediate(true);
    txtFoldername.addStyleName(ValoTheme.TEXTFIELD_SMALL);
    
    createNow = new Button("OK");
    createNow.addStyleName(ValoTheme.BUTTON_SMALL);
    
    createNow.addClickListener(new ClickListener() { 
      private static final long serialVersionUID = 1L;

      @Override
      public void buttonClick(ClickEvent event) {
        if(txtFoldername.getValue().isEmpty()) {
          Notification.show("Attention", "Folder name cannot be empty", Type.WARNING_MESSAGE);
          return;
        }
        
        try {
          controller.createFolder(txtFoldername.getValue());
          RaidNotificationUtil.showSuccess("Folder: " + txtFoldername.getValue() + " created");
        } catch (Exception e) {
          RaidNotificationUtil.showError("Could not create Folder: " + txtFoldername.getValue());
        }
        setVisible(false);
        rootView.ls();
      }
    });

    addComponent(label);
    addComponent(txtFoldername);
    addComponent(createNow);
  }
}
