package cloud.box;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import utils.RaidUtils;
import cloud.BasicCloudBaseHandler;
import cloud.content.RaidContent;
import cloud.entry.EntryWrapper;

import com.box.boxjavalibv2.BoxClient;
import com.box.boxjavalibv2.authorization.OAuthRefreshListener;
import com.box.boxjavalibv2.dao.BoxCollection;
import com.box.boxjavalibv2.dao.BoxFolder;
import com.box.boxjavalibv2.dao.BoxItem;
import com.box.boxjavalibv2.dao.BoxTypedObject;
import com.box.boxjavalibv2.dao.IAuthData;
import com.box.boxjavalibv2.exceptions.AuthFatalFailureException;
import com.box.boxjavalibv2.exceptions.BoxServerException;
import com.box.boxjavalibv2.requests.requestobjects.BoxFolderDeleteRequestObject;
import com.box.boxjavalibv2.requests.requestobjects.BoxFolderRequestObject;
import com.box.restclientv2.exceptions.BoxRestException;
import com.box.restclientv2.requestsbase.BoxFileUploadRequestObject;

@SuppressWarnings("javadoc")
public class BoxHandler extends BasicCloudBaseHandler {
	
	private final Logger logger = Logger.getLogger(this.getClass().getName());

  private BoxClient client;
  private BoxUtils boxUtils;
  
  public BoxHandler() throws AuthFatalFailureException, IOException {
  	init();
  	logger.info(getID() + " initialized successfully");
  }

  @Override
	public RaidUtils.HandlerID getID() {
		return RaidUtils.HandlerID.BOX;
	}
  
  @Override
  public void init() throws AuthFatalFailureException, IOException {
  	client = BoxAuthenticator.authenticate();  	
  	boxUtils = new BoxUtils(client);
  	
  	client.addOAuthRefreshListener(new OAuthRefreshListener() {
      @Override
      public void onRefresh(IAuthData newAuthData) {      	
      	logger.warn("received new OAuth refresh event");
      	//TODO
      }
  	});	
  }
  
  @Override
  public void update(final String path, RaidContent raidContent) throws Exception {
  	if(path == null || raidContent == null || raidContent.getBytes() == null) {
			logger.warn("null values not allowed");
			return;
		}
  	
  	BoxItem response = null;  
  	EntryWrapper entry = null;
  	
  	if((entry = getEntryInfo(path)) == null)
  		return;
  		
		BoxFileUploadRequestObject uploadRequestObject = BoxFileUploadRequestObject
				.uploadNewVersionRequestObject(entry.getFilename(), new ByteArrayInputStream(raidContent.getBytes()));
		response = client.getFilesManager().uploadNewVersion(entry.getId(), uploadRequestObject);

  	if(response != null) {
			logger.info("cloud entry updated successfully - " + path);
			return;
		}
		logger.error("error updating cloud entry - " + path);		
  }

  @Override
  public void create(final String path, final RaidContent raidContent) throws Exception {  
  	if(path == null) {
  	  logger.warn("null values not allowed");
  	  return;
  	}
  	
  	BoxItem response = null;  
  	EntryWrapper parentEntry = null;
  	
  	if(getEntryInfo(path) != null) {
  	  update(path, raidContent);
  	  return;
  	}

  	if((parentEntry = getParentEntryInfo(path)) == null)
  		return;
  	
  	String entryName = Paths.get(path).getFileName().toString();
  	
  	if(raidContent == null || raidContent.getBytes() == null) {
  		BoxFolderRequestObject requestObject = BoxFolderRequestObject
  				.createFolderRequestObject(entryName, parentEntry.getId());
  		response = client.getFoldersManager().createFolder(requestObject);
  	}
  	else {
  		BoxFileUploadRequestObject uploadRequestObject = BoxFileUploadRequestObject
  				.uploadFileRequestObject(parentEntry.getId(), entryName, new ByteArrayInputStream(raidContent.getBytes()));
  		response = client.getFilesManager().uploadFile(uploadRequestObject);
  	}
  	
  	if(response != null) {
			logger.info("cloud entry created successfully - " + path);
			return;
		}
		logger.error("error creating cloud entry - " + path);		
  }

  @Override
  public void delete(final String path) throws BoxRestException, BoxServerException, AuthFatalFailureException {
  	if(path == null) {
			logger.warn("path cannot be null");
			return;
		}
  	
  	EntryWrapper entry = null;  	
  	if((entry = getEntryInfo(path)) == null)
  		return;

		if(entry.isFile()) {
  		client.getFilesManager().deleteFile(entry.getId(), null);    		
		}
  	else {
  		BoxFolderDeleteRequestObject requestObj = BoxFolderDeleteRequestObject.deleteFolderRequestObject(true);
  		client.getFoldersManager().deleteFolder(entry.getId(), requestObj);
  	}
		logger.info("cloud entry deleted successfully - " + path);
	}

  @Override
  public RaidContent read(final String path, final String rev) throws BoxRestException, BoxServerException, AuthFatalFailureException, IOException  {
  	if(path == null) {
			logger.warn("path cannot be null");
			return null;
		}
  	
  	EntryWrapper entry = null;  	
  	if((entry = getEntryInfo(path)) == null)
  		return null;
  	
  	InputStream in = null;
  	logger.info("start downloading - " + path);
  	try {
  		in = client.getFilesManager().downloadFile(entry.getId(), null);
  		logger.info("cloud entry downloaded successfully - " + path);
  		return RaidUtils.getRaidContentFromCloud( IOUtils.toByteArray(in), this );
  	} finally {
  	  if(in != null)
  		  in.close();
  	}
  }

  @Override
  public InputStream startRead(final String id, final String rev) throws BoxRestException, BoxServerException, AuthFatalFailureException {
  	if(id == null) {
  	  logger.warn("id cannot be null");
  	  return null;
  	}	 	
  	return client.getFilesManager().downloadFile(id, null);
  }

	@Override
	public EntryWrapper getEntryInfo(final String path) throws BoxRestException, BoxServerException, AuthFatalFailureException {
		if(path == null) {
			logger.warn("path cannot be null");
			return null;
		}

		BoxItem item = null;
		if((item = boxUtils.getItem(path, false)) != null)
			return new EntryWrapper(item, path);

		logger.debug("cloud entry not found - " + path);	
		return null;
	}
	
	public EntryWrapper getParentEntryInfo(final String path) throws BoxRestException, BoxServerException, AuthFatalFailureException {
		if(path == null) {
			logger.warn("path cannot be null");
			return null;
		}
		
		BoxItem parentItem = null;
		if((parentItem = boxUtils.getItem(path, true)) != null)
			return new EntryWrapper(parentItem, path);

		logger.debug("cloud parent entry not found - " + path);	
		return null;
	}

	@Override
	public List<EntryWrapper> list(final String path, boolean setRaidMode) throws Exception {		
		
		BoxItem item = null;		
		if((item = boxUtils.getItem(path, false)) == null)
		  return null;

		List<EntryWrapper> entries = new ArrayList<EntryWrapper>();
		String dir = path.isEmpty() ? path : path + "/";

		if(item instanceof BoxFolder) {
			BoxCollection collection = ((BoxFolder) item).getItemCollection();
			if(collection != null) {
			  for(BoxTypedObject object : collection.getEntries()) {
			    if(object instanceof BoxItem) {
			      String filePath =  dir + ((BoxItem)object).getName();
			      EntryWrapper ew = getEntryInfo(filePath);
						
			      if (setRaidMode && ew.isFile())
			        ew.setRaidMode(getRaidMode(ew.getId(), null));
			      entries.add(ew);
			    }
			  }
			}
		}
		return entries;
	}
}
