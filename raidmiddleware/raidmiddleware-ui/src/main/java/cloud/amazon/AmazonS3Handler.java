package cloud.amazon;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import utils.RaidUtils;
import cloud.BasicCloudBaseHandler;
import cloud.config.Config;
import cloud.content.RaidContent;
import cloud.entry.EntryWrapper;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

@SuppressWarnings("javadoc")
public class AmazonS3Handler extends BasicCloudBaseHandler {
	
	private final Logger logger = Logger.getLogger(this.getClass().getName());

	private AmazonS3Client client;
	
	public AmazonS3Handler() throws Exception {
		init();
  	logger.info(getID() + " initialized successfully");
	}
	
	@Override
	public RaidUtils.HandlerID getID() {
		return RaidUtils.HandlerID.AMAZON;
	}

	@Override
	public void init() throws Exception {
		client = AmazonS3Authenticator.authenticate();
		
    if(!client.doesBucketExist(Config.Amazon.BUCKET_NAME)) {
    	client.createBucket(Config.Amazon.BUCKET_NAME);
    }
    
    // Make the bucket version-enabled.
    // enableVersioningOnBucket(s3Client, Config.Amazon.BUCKET_NAME); //for stage 2
	}
	
  @Override
  public void update(final String name, final RaidContent content) throws IOException {
      this.create(name, content); // overrides the existing file
  }
  
	@Override
  public void create(final String path, final RaidContent content) throws IOException {
		
		boolean update = false;
		
		if(path == null) {
			logger.warn("null values not allowed");
			return;
		}
		
  	if(getEntryInfo(path) != null) {
  	  update = true;
  	}
		
		PutObjectResult response = null;
		
		if(content == null || content.getBytes() == null) {
			String folderPath = !path.endsWith("/") ? path + "/" : path;
    	ObjectMetadata metadata = new ObjectMetadata();
    	metadata.setContentType("binary/octet-stream");  
    	metadata.setContentLength(0L);
    	response = client.putObject(Config.Amazon.BUCKET_NAME, folderPath, new ByteArrayInputStream(new byte[0]), metadata);
    }
		else {
    	ObjectMetadata metaData = new ObjectMetadata();
    	metaData.setContentLength(content.getBytes().length);        
    	response = client.putObject(Config.Amazon.BUCKET_NAME, path, new ByteArrayInputStream(content.getBytes()), metaData);
		}
    
    if(response != null) {
    	if(update)
    		logger.info("cloud entry updated successfully - " + path);
    	else
    		logger.info("cloud entry created successfully - " + path);
			return;
		}
		logger.error("error creating cloud entry - " + path);		   
  }

  @Override
  public void delete(final String path) throws Exception {
  	if(path == null) {
			logger.warn("path cannot be null");
			return;
		}
  	
  	if (getEntryInfo(path) == null) {
  		logger.info("cloud entry not found - " + path);
  		return;
  	}

    try {
    	if(path.endsWith("/")) {
    		deleteAllItemsInFolder(path); //delete folder key and all sub items
    	}
    	else {
    		client.deleteObject(Config.Amazon.BUCKET_NAME, path);  		
    	}
    	logger.info("cloud entry deleted successfully - " + path);
    } catch(AmazonClientException e){
    	logger.error("error deleting cloud entry - " + path, e);
    }
  }

  @Override
  public RaidContent read(final String path, final String rev) throws IOException {
  	if(path == null) {
			logger.warn("path cannot be null");
			return null;
		} 
  	
    S3Object object = null;
    try {
    	logger.info("start downloading - " + path);
    	object = client.getObject(Config.Amazon.BUCKET_NAME, path);
    	logger.info("cloud entry downloaded successfully - " + path);
    	if(object != null) {
    		byte[] fileContent = IOUtils.toByteArray(object.getObjectContent());
    		return RaidUtils.getRaidContentFromCloud(fileContent, this);
    	}
    	return null;
    }
    finally {
    	if (object != null) {
    		object.close();
    	}
    }   
  }

  @Override
  public InputStream startRead(final String path, final String rev) throws IOException {
  	if(path == null) {
			logger.warn("path cannot be null");
			return null;
		} 
  	
    S3Object object = null;
//    try {
    	//logger.info("start downloading - " + path);
    	object = client.getObject(Config.Amazon.BUCKET_NAME, path);
    	//logger.info("cloud entry downloaded successfully - " + path);
    	if(object != null)
    		return object.getObjectContent();
    	return null;
//    }
//    finally {
//    	if (object != null) {
//    		object.close();
//    	}
//    } 
  }

  @Override
  public EntryWrapper getEntryInfo(final String path) {
  	if(path == null) {
			logger.warn("path cannot be null");
			return null;
		} 

    try {
    	ObjectMetadata metadata = client.getObjectMetadata(Config.Amazon.BUCKET_NAME, path);
    	EntryWrapper ew = new EntryWrapper(metadata, path);
    	return ew;
    } catch (Exception e) {
			//
		}
    return null;
  }


	@Override
	public List<EntryWrapper> list(final String path, boolean setRaidMode) throws Exception {			
		final List<EntryWrapper> entries = new ArrayList<EntryWrapper>();
		final String dir = path.isEmpty() ? "" : path + "/";
		
		final ListObjectsRequest listObjectRequest = new ListObjectsRequest()		
		    .withBucketName(Config.Amazon.BUCKET_NAME)
		    .withPrefix(dir)
		    .withDelimiter("/"); //without folders

		ObjectListing objectListing = client.listObjects(listObjectRequest);

		if(objectListing.getObjectSummaries().size() == 0 && objectListing.getCommonPrefixes().size() == 0)
			return null;
		
		do {
    	for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
    		if(!objectSummary.getKey().equals(dir)) {
    			EntryWrapper e = getEntryInfo(objectSummary.getKey());
    			
    			if(e != null) {
    				if (setRaidMode && e.isFile())
          		e.setRaidMode(getRaidMode(objectSummary.getKey(), null));
	    			entries.add(e);
    			}	else
	    			System.err.println("AmazonS3: file " + objectSummary.getKey() + " not found.");
    		}
    	}
    	
    	for(String folder : objectListing.getCommonPrefixes()) {
    		EntryWrapper e = getEntryInfo(folder);
    		if(e != null)
    			entries.add(e);
    		else
    			System.err.println("AmazonS3: folder " + folder + " not found.");
			}
    	
    	objectListing = client.listNextBatchOfObjects(objectListing);
		} while (objectListing.isTruncated());
		
		return entries;
	}
		
	private void deleteAllItemsInFolder(final String key) {
  	final ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
  	.withBucketName(Config.Amazon.BUCKET_NAME)
  	.withPrefix(key);

    ObjectListing objects = client.listObjects(listObjectsRequest);
    do {
    	for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
    		client.deleteObject(Config.Amazon.BUCKET_NAME, objectSummary.getKey());
    	}
    	objects = client.listNextBatchOfObjects(objects);
    } while (objects.isTruncated());
  }
}