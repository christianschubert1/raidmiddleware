package cloud;
import java.io.InputStream;

import cloud.content.RaidContent;
import cloud.entry.EntryWrapper;
import cloud.interfaces.ICloudBaseHandler;


public abstract class BasicCloudBaseHandler implements ICloudBaseHandler {

	@Override
	public int getRaidMode(String id, String rev) throws Exception {
		byte mnr = readMagicNumber(id, rev);
		if (mnr == RaidContent.RAID5_MAGIC_NUMBER)
			return EntryWrapper.RAID_5_MODE;
		else
			return EntryWrapper.RAID_1_MODE;
	}
	
	private byte readMagicNumber(String id, String rev) throws Exception {
		byte[] firstByte = new byte[1];
		InputStream is = startRead(id, rev);
		is.read(firstByte);
		is.close();
		return firstByte[0];
	}

}
