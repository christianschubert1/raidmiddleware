package cloud.entry;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("javadoc")
public class FileCompareWrapper implements Comparable<FileCompareWrapper>, Serializable {

	private static final long serialVersionUID = -2759308511523930922L;
	
	//protected final String id;
	//protected final String name;
	protected final String path;
	protected Long bytes;
	//protected String filesize;
	protected Date lastModified;
	//protected String revision;
	
	public FileCompareWrapper(final String id, final Long bytes, Date lastModified) {
		this.path = id;
		this.bytes = bytes;
		this.lastModified = lastModified;
  }
 
	public String getPath() {
		return path;
	}
	
	public Long getBytes() {
		return bytes;
	}
	
	public Date getLastModified() {
		return lastModified;
	}

	@Override
	public int compareTo(FileCompareWrapper o) {
		if(path.equals(o.getPath()) && bytes == o.getBytes())// && lastModified.equals(o.getLastModified()))
			return 0;
		return -1;
	}
}
