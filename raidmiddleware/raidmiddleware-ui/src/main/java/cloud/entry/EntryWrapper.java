package cloud.entry;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.box.boxjavalibv2.dao.BoxItem;
import com.dropbox.core.DbxEntry;

@SuppressWarnings("javadoc")
public class EntryWrapper implements Serializable {

	private static final long serialVersionUID = -2759308511523930922L;
	
	public static final int RAID_UNDEFINED_MODE = -1;
	public static final int RAID_1_MODE = 1;
	public static final int RAID_5_MODE = 5;
	
	
	protected String id;
	public String getId() {
		return id;
	}

	protected String name;
	protected String path;
	protected Long bytes;
	protected Date lastModified;
	protected String revision;	
	protected String iconName;
	protected boolean isFile;
	protected String owner;
	protected List<Integer> versions;
	protected int raidMode = RAID_UNDEFINED_MODE;
	
	public String getOwner() {
		return owner;
	}

	public EntryWrapper(final DbxEntry dbxEntry) {
		this.owner = "Dropbox";
		this.id = this.path = dbxEntry.path.substring(1);
    this.name = dbxEntry.name;
    this.isFile = dbxEntry.isFile();
    this.iconName = dbxEntry.iconName;

    if(dbxEntry.isFile()) {
      DbxEntry.File dbxFile = dbxEntry.asFile();
      this.bytes = dbxFile.numBytes;
      this.lastModified = dbxFile.lastModified;
      this.revision = dbxFile.rev;
    } else {
			this.name += "/";
			this.id = this.path += "/";
			this.bytes = 0L;
		}
  } 
	
	public EntryWrapper(final BoxItem boxItem, final String path) {
		this.owner = "Box";
		this.id = boxItem.getId();
		this.name = boxItem.getName();
		this.path = path;		
		this.bytes = boxItem.getSize() != null ? boxItem.getSize().longValue() : 0L;
		this.lastModified = boxItem.dateModifiedAt();
		this.revision = boxItem.getEtag();
		this.isFile = boxItem.getType().equals("file");
		
		if(!isFile) {
			this.name += "/";
			this.path = path + "/";
		}
  }

//    public EntryWrapper(final S3Object entry, final String path) {
//        ObjectMetadata meta = entry.getObjectMetadata();
//        this.id = path;
//        this.name = new File(entry.getKey()).getName();
//        this.path = path;
//        this.isFile = true; // TODO?
//        this.lastModified = meta.getLastModified();
////        this.revision = meta.getETag();
//        this.revision = meta.getVersionId();
//        this.bytes = meta.getContentLength();
//    }
    
  public EntryWrapper(final ObjectMetadata meta, final String path) { 
  	this.owner = "AmazonS3";
  	this.id = this.path = path;
  	this.isFile = true;
  	this.name = new File(path).getName();
  	this.lastModified = meta.getLastModified();
  	this.revision = meta.getVersionId();
    this.bytes = meta.getContentLength();
  	
    if(path.endsWith("/")) {   		
  		this.name += "/";
			this.bytes = 0L;
  		this.isFile = false;
  	} 
  }

    public String getPath() {
		return path;
	}
	
	public String getFilename() {
		return name;
	}
	
	/**
	 * Get the size in bytes
	 * @return size in bytes
	 */
	public Long getSize() {
		return bytes;
	}
	
	public Date getLastModified() {
		return lastModified;
	}
	
	public String getRevision() {
		return revision;
	}

	public boolean isFile() {
		return isFile;
	}

	@Override
	public String toString() {
		return "{\n" +
							(isFile ? "File: " : "Folder: ") + name + "\n" +
							"Path: " + path + "\n" + 
							"Id: " + id + "\n" +
							"Size: " + bytes + "\n" + 
							"Last Modified: " + lastModified + "\n" +
							"Revision: " + revision + "\n" + 
						"}\n"	;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof EntryWrapper))
			return false;

		EntryWrapper that = (EntryWrapper) other;
		// TODO add revision for equality check
		return this.getFilename().equals(that.getFilename()) && this.getPath().equals(that.getPath()) &&
				this.getSize().equals(that.getSize()) && this.getLastModified().equals(that.getLastModified());
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	
	public void setRaidMode(int raidMode) {
		this.raidMode = raidMode;
	}
	
	public int getRaidMode() {
		return raidMode;
	}
}
