package cloud.interfaces;

import java.nio.file.Path;

@SuppressWarnings("javadoc")
public interface IRaidController {
	public void createRaid1Entry(final String path, final byte[] content);
	
	public void createRaid5Entry(final String path, final byte[] content);
	
	public void updateRaid1Entry(final String path, final byte[] content);
	
	public void updateRaid5Entry(final String path, final byte[] content);
	
	public byte[] readEntry(final String path);
	
	public void deleteEntry(final String path);
	
	public void listDirectory(final String path, boolean detail);
	
	public Path navigateTo(final String path);
	
	public Path navigateBack();
}
