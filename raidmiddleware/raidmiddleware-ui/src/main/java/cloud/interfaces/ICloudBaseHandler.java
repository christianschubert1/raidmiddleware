package cloud.interfaces;

import java.io.InputStream;
import java.util.List;

import utils.RaidUtils;
import cloud.content.RaidContent;
import cloud.entry.EntryWrapper;

@SuppressWarnings("javadoc")
public interface ICloudBaseHandler {
	
	public RaidUtils.HandlerID getID();
	
	public void init() throws Exception;
	
	public void create(final String path, final RaidContent content) throws Exception;
	
	public void delete(final String path) throws Exception;
	
	public RaidContent read(final String path, final String rev) throws Exception;
	
	public InputStream startRead(final String path, final String rev) throws Exception;
	
	public void update(final String path, RaidContent content) throws Exception;
	
	public EntryWrapper getEntryInfo(final String path) throws Exception;
	
	public List<EntryWrapper> list(final String dir, boolean setRaidMode) throws Exception;
	
	public int getRaidMode(final String path, final String rev) throws Exception;
}
