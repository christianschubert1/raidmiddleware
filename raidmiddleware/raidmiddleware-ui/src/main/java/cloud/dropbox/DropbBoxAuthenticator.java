package cloud.dropbox;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Locale;

import main.CloudProps;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import cloud.config.Config;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuthNoRedirect;

@SuppressWarnings("javadoc")
public class DropbBoxAuthenticator {
	
	private static final Logger logger = LogManager.getLogger(DropbBoxAuthenticator.class.getName());

	private DropbBoxAuthenticator() {
		//never call
	}
	
  public static DbxClient authenticate() throws IOException, DbxException {
    final DbxRequestConfig config = new DbxRequestConfig(Config.CLIENT_ID, Locale.getDefault().toString());
    final String token = CloudProps.getInstance().getDropBoxAccessToken();

    try {
      if(token != null) 
        return new DbxClient(config, token);
    }
    catch (Exception e) {
    	logger.warn("Token maybe expired. Trying to get a new one..", e);
    }   
    return new DbxClient(config, webAuth(config));
  }
  
  private static String webAuth(final DbxRequestConfig config) throws IOException, DbxException {   
    final DbxAppInfo appInfo = new DbxAppInfo(Config.DropBox.APP_KEY, Config.DropBox.APP_SECRET);
    final DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config, appInfo);

    logger.info("1. Click \"Allow\" (you might have to log in first)");
    logger.info("2. Copy the authorization code");
    logger.info("2. Paste the copied code here: ");

    final String authorizeUrl = webAuth.start();
    Desktop.getDesktop().browse(URI.create(authorizeUrl));
    final String code = new BufferedReader(new InputStreamReader(System.in)).readLine().trim();   
    final DbxAuthFinish authFinish = webAuth.finish(code);
    CloudProps.getInstance().setDropBoxAccessToken(authFinish.accessToken);
    return authFinish.accessToken;
  }
}