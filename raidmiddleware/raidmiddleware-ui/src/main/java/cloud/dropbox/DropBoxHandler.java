package cloud.dropbox;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import utils.RaidUtils;
import cloud.BasicCloudBaseHandler;
import cloud.config.Config;
import cloud.content.RaidContent;
import cloud.entry.EntryWrapper;

import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxEntry;
import com.dropbox.core.DbxEntry.WithChildren;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxWriteMode;

@SuppressWarnings("javadoc")
public class DropBoxHandler extends BasicCloudBaseHandler {
	
	private final Logger logger = LogManager.getLogger(this.getClass().getName());		
	private DbxClient client;
	
	public DropBoxHandler() throws DbxException, IOException {
		init();
		logger.info(getID() + " initialized successfully");
	}
	
	@Override
	public RaidUtils.HandlerID getID() {
		return RaidUtils.HandlerID.DROPBOX;
	}
	
	@Override
	public void init() throws DbxException, IOException {	  
		if(client == null)
			client = DropbBoxAuthenticator.authenticate();
	}
	
	@Override
	public void update(final String path, RaidContent raidContent) throws Exception {	
		if(path == null || raidContent == null || raidContent.getBytes() == null) {
			logger.warn("null values not allowed");
			return;
		}
		
		if (getEntryInfo(path) == null) {
  		return;
  	}
		
		DbxEntry.File file = null;
		
		final ByteArrayInputStream iStream = new ByteArrayInputStream(raidContent.getBytes());
		
		try {
			file = client.uploadFile(Config.DropBox.ROOT_FOLDER + path, 
					DbxWriteMode.force(), raidContent.getBytes().length, iStream);
		} finally {
			iStream.close();
		} 
		
		if(file != null) {
			logger.info("cloud entry updated successfully - " + path);	
			return;
		}
		logger.error("error updating cloud entry - " + path);
	}
	
	@Override
	public void create(final String path, final RaidContent raidContent) throws Exception {		
	  if(path == null) {
	    logger.warn("path cannot be null");
	    return;
	  }
		
	  if (getEntryInfo(path) != null) {
	    update(path, raidContent);
	    return;
	  }
		
		DbxEntry response = null;
		String dropboxPath = path.endsWith("/") ? path.substring(0, path.length()-1) : path;

		if(raidContent == null || raidContent.getBytes() == null) {
			response = client.createFolder(Config.DropBox.ROOT_FOLDER + dropboxPath);
		}
		else {		
			ByteArrayInputStream iStream = new ByteArrayInputStream(raidContent.getBytes());						
			try {			
				response = client.uploadFile(Config.DropBox.ROOT_FOLDER + dropboxPath, 
						DbxWriteMode.force(), raidContent.getBytes().length, iStream);				
			} finally {
				iStream.close();
			}	
		}
		
		if(response != null) {
			logger.info("cloud entry created successfully - " + path);
			return;
		}
		logger.error("error creating cloud entry - " + path);		
	}
	
	@Override
	public void delete(final String path) throws DbxException {
		if(path == null) {
			logger.warn("path cannot be null");
			return;
		}

		String dropboxPath = path.endsWith("/") ? path.substring(0, path.length()-1) : path;
		
		if (getEntryInfo(dropboxPath) == null) {
  		return;
  	}
		
		try {
			client.delete(Config.DropBox.ROOT_FOLDER + dropboxPath);
			logger.info("cloud entry deleted successfully - " + dropboxPath);
		} catch (DbxException e) {
			logger.error("error deleting cloud entry - " + dropboxPath, e);
		}
	}
	
	@Override
	public RaidContent read(final String path, final String rev) throws DbxException, IOException {
		if(path == null) {
			logger.warn("path cannot be null");
			return null;
		}
		
		if (getEntryInfo(path) == null) {
  		return null;
  	}
		
		DbxEntry.File file = null;
		ByteArrayOutputStream oStream = new ByteArrayOutputStream();
		
		try {
			logger.info("start downloading - " + path);
			file = client.getFile(Config.DropBox.ROOT_FOLDER + path, rev, oStream);
			if(file != null) {
				logger.info("cloud entry downloaded successfully - " + path);
				return RaidUtils.getRaidContentFromCloud( oStream.toByteArray(), this );
			}		
		} finally {
			oStream.close();
		}

		logger.error("error downloading cloud entry - " + path);		
		return null;
	}
	
	@Override
	public InputStream startRead(final String path, final String rev) throws DbxException, IOException {
		if(path == null) {
			logger.warn("path cannot be null");
			return null;
		}
		
		if (getEntryInfo(path) == null) {
  		return null;
  	}
		
		DbxClient.Downloader downloader = client.startGetFile(Config.DropBox.ROOT_FOLDER + path, rev);
		
		if(downloader != null) {
//			try {
				return downloader.body;
//			}
//			finally {
//				downloader.close();
//			}
		}
		return null;
	}
	
	@Override
	public EntryWrapper getEntryInfo(final String path) throws DbxException {
		if(path == null) {
			logger.warn("path cannot be null");
			return null;
		}

		DbxEntry entry = client.getMetadata(Config.DropBox.ROOT_FOLDER + path);
		
		if(entry != null) {
			return new EntryWrapper(entry);
		}
		logger.error("cloud entry not found - " + path);	
		return null;
	}

	@Override
	public List<EntryWrapper> list(final String path, boolean setRaidMode) throws Exception {
		if(path == null) {
			logger.warn("path cannot be null");
			return null;
		}
		
		List<EntryWrapper> entries = null;
		String unixPath = FilenameUtils.separatorsToUnix(Paths.get(path).toString());	
		WithChildren listWithChildren = client.getMetadataWithChildren(Config.DropBox.ROOT_FOLDER + unixPath);
		
		if(listWithChildren != null) {
			entries = new ArrayList<EntryWrapper>();
			if(listWithChildren.children != null) {
				for(DbxEntry entry : listWithChildren.children) {
					EntryWrapper ew = new EntryWrapper(entry);
					if (setRaidMode && ew.isFile())
						ew.setRaidMode(getRaidMode(entry.path, null));
					entries.add(ew);
				}
			}
		}
		return entries;
	}
}