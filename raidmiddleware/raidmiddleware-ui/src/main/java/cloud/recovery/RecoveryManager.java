package cloud.recovery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.vaadin.ui.UI;

import utils.Raid5Utils;
import vaadinui.RaidNotificationUtil;
import cloud.content.Raid1Content;
import cloud.content.Raid5Fragment;
import cloud.content.RaidContent;
import cloud.interfaces.ICloudBaseHandler;

@SuppressWarnings("javadoc")
public class RecoveryManager {
	
	private static final Logger logger = LogManager.getLogger(RecoveryManager.class.getName());
	private static RecoveryManager INSTANCE;
	private static RaidContent faultyRaidContent;
	private static int faultyHandlerID;
	
	// Singelton
	private RecoveryManager() {
	}
	
	public static RecoveryManager getInstance() {
		if (INSTANCE == null) 
			INSTANCE = new RecoveryManager();
		
		return INSTANCE;
	}
	
	/**
	 * Replaces a faulty replica by a "good" one.
	 * @param faultyHandler The faulty cloud handler.
	 * @param validHandler The valid (= "good") cloud handler.
	 * @param path The path to the file.
	 * @return Returns true if replacement was successfully. Otherwise it returns false.
	 */
	private boolean replaceFaultyReplica(List<RaidContent> faultyContents, List<RaidContent> validContents, String path) {
		RaidContent faultyContent = faultyContents.get(0);
		ICloudBaseHandler faultyHandler = faultyContent.getHandler();
		logger.info("recovery manager found faulty entry on " + faultyHandler.getID().getName());
		
		logger.info("start recovery..");
		RaidNotificationUtil.showInfo("Found faulty entry on " + faultyHandler.getID().getName() + "</br>start recovering..");
		
		try {
			RaidContent validContent = validContents.get(0);
			// raid 5
			if (validContents.get(0) instanceof Raid5Fragment) {
				
				if (validContents.size() < 2) {
					// can not recover from only one RAID5 file
					return false;
				}
				
				Raid5Fragment fragment1 = (Raid5Fragment) validContent;
				Raid5Fragment fragment2 = (Raid5Fragment) validContents.get(1);
				byte[] recoveredRaid5Fragment = Raid5Utils.recoverMissingFragment(fragment1.getContent(), fragment2.getContent());
				
				if (faultyContent instanceof Raid1Content) {
					Raid5Fragment newRaid5Fragment = new Raid5Fragment(faultyHandler);
					faultyContent = newRaid5Fragment;
				}
				faultyContent.setContent(recoveredRaid5Fragment);
				faultyContent.setHash(fragment1.getHash());
			// raid 0
			} else {
				if (faultyContent instanceof Raid5Fragment) {
					Raid1Content newRaid1Content = new Raid1Content(faultyHandler);
					faultyContent = newRaid1Content;
				}				
				faultyContent.setContent(validContent.getContent());
				faultyContent.setHash(validContent.getHash());	
				
			}
			
			if(faultyHandler.getEntryInfo(path) != null)
				faultyHandler.update(path, faultyContent);
			else
				faultyHandler.create(path, faultyContent);
			
			faultyRaidContent = faultyContent;
			faultyHandlerID = faultyHandler.getID().ordinal();
			
			logger.info("recovery finished successful");
			RaidNotificationUtil.showSuccess("Recovery finished successful");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("recovery finished with errors");
		return false;
	}
	
	/**
	 * Checks if all cloud handlers share the same file. Compares the hash values of the files content. 
	 * If all have the same value, it does nothing. If one has an incorrect value, 
	 * it restores the content from one of the other two correct cloud services.
	 * @param boxHandler The cloud handler for the Box repository.
	 * @param dropBoxHandler The cloud handler for the DropBox repository.
	 * @param amazonHandler The cloud handler for the Amazon repository.
	 * @param path The path to the file.
	 * @return Returns true if recovery not necessary (all files are the same) or was successfully. 
	 * 		Otherwise returns false when all files differ from each other or are missing. 
	 */
	public boolean recover(List<RaidContent> raidContents, String path) {
		faultyRaidContent = null;
		List<RaidContent> cloudContentAvail = new ArrayList<RaidContent>();
		List<RaidContent> cloudContentMissing = new ArrayList<RaidContent>();
		boolean success = true;		
		
		for (RaidContent raidContent : raidContents) {
			if (raidContent == null || raidContent.getBytes() == null)
				cloudContentMissing.add(raidContent);
			else
				cloudContentAvail.add(raidContent);
		}

		if (cloudContentAvail.size() == raidContents.size()) {
			// case 1: all clouds have files -> compare 3 hash values			
			int invalidPosition = -1;
			if (raidContents.size() == 3) {
				invalidPosition = compare3HashCodes(
					cloudContentAvail.get(0).getHash(), 
						cloudContentAvail.get(1).getHash(), 
							cloudContentAvail.get(2).getHash()
							);
			}
			if (invalidPosition >= 0) {
				// case 1.1: two hash values are the same, one differs (= faulty) 
				// -> replace the faulty one by one of the good ones
				RaidContent faultyContent = cloudContentAvail.get(invalidPosition);
				List<RaidContent> validContents = new ArrayList<RaidContent>(cloudContentAvail);
				List<RaidContent> faultyContents = new ArrayList<RaidContent>();
				faultyContents.add(faultyContent);
				validContents.remove(invalidPosition);
				
				success &= replaceFaultyReplica(faultyContents, validContents, path);
			} else if (invalidPosition == -2) {
				// case 1.2: all hash values differ -> no way to compromise, to indicate the "good" file
				success = false;
			} else {
				// case 1.3: hash values are all the same -> everything OK, nothing to do
				success = true;
			}
		} else if (cloudContentMissing.size() == 1) {	
		  
			// case 2: one cloud has missing file
			RaidContent faultyContent = cloudContentMissing.get(0);
			List<RaidContent> faultyContents = new ArrayList<RaidContent>();
			faultyContents.add(faultyContent);			
			if (cloudContentAvail.size() == 1) {
				// case 2.1: only one cloud has file available -> replace faulty by available file (ignore read error from third cloud)
				success &= replaceFaultyReplica(faultyContents, cloudContentAvail, path);
			} else if (cloudContentAvail.size() == 2) {		
				// case 2.2: two clouds have	 files available -> compare 2 hash values
				if (cloudContentAvail.get(0).getHash() == cloudContentAvail.get(1).getHash()) {
				  System.err.println(faultyContent);
					// case 2.2.1: two hash values are the same -> replace faulty by one of the good ones
					success &= replaceFaultyReplica(faultyContents, cloudContentAvail, path);
				} else {
					// case 2.2.2: hash values differ from each other -> no way to compromise
					success = false;
				}
			}
		} else if (cloudContentMissing.size() == 2) {
			// case 3: two clouds have missing files 
				if (cloudContentAvail.size() == 1) {
					// case 3.1: one file available -> replace the missing files by the one who is available
					RaidContent validContent = cloudContentAvail.get(0);
					List<RaidContent> faultyContents = new ArrayList<RaidContent>();
					faultyContents.add(cloudContentMissing.get(0));
					success &= replaceFaultyReplica(faultyContents, Arrays.asList(validContent), path);
					
					
					faultyContents = new ArrayList<RaidContent>();
					faultyContents.add(cloudContentMissing.get(1));
					success &= replaceFaultyReplica(faultyContents, Arrays.asList(validContent), path);
				} else {
					// case 3.2: no files available (one cloud has a read error) -> no way to compromise
					success = false;
				}
		} else if (cloudContentMissing.size() == 3) {
			// case 4: no cloud has a file available -> no way to compromise
			success = false;
		} else {
			// case 5: one or two clouds have files available, but at least one read error occurred -> no way to compromise
			// case 6: all clouds had a read error -> no way to compromise
			success = false;
		}
					
		return success;
	}
	
//	public byte[] getRecoveryConent() {
//		if(cloudFileAvail == null || cloudFileAvail.isEmpty())
//			return null;
//		return cloudFileAvail.get(cloudFileAvail.size()-1).getContent();
//	}
	
	/**
	 * Compares 3 given hash values
	 * @param first
	 * @param second
	 * @param third
	 * @return Returns the position of the not identical hash value. Returns 0 if all values are the same or -1 if all are not the same.
	 */
	private int compare3HashCodes(long first, long second, long third) {
		if (second == first) {
			if (third == second) {
				return -1; // all the same
			}
			return 2; // third invalid
		} else if (second == third) { 
			return 0; // first invalid			
		} else if (first == third) { 
			return 1; // second invalid
		}
		return -2; // all not the same
	}
	
	public RaidContent getFaultyRaidContent() {
		return faultyRaidContent;
	}
	
	public int getFaultyID() {
		return faultyHandlerID;
	}
}
