package cloud.recovery;

import java.util.zip.CRC32;

public class RecoveryUtils {
	
	public static long computeCRCChecksum(byte[] value) {
		CRC32 crc = new CRC32();
		crc.update(value);
		return crc.getValue();
	}
}
