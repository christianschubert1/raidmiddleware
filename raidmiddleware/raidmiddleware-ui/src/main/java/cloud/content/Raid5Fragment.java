package cloud.content;

import java.nio.ByteBuffer;
import java.util.Arrays;

import cloud.interfaces.ICloudBaseHandler;

// magic number + hash value + content
public class Raid5Fragment extends RaidContent {
	
	public Raid5Fragment(ICloudBaseHandler handler) {
		super(handler);
	}

	public final static int HASH_OFFSET = 8;
	
	@Override
	public void setContent(byte[] content) {
		bytes = new byte[content.length + MAGIC_NUMBER_SIZE + HASH_OFFSET];
		bytes[0] = RAID5_MAGIC_NUMBER;
					
		int bytesIndex = MAGIC_NUMBER_SIZE + HASH_OFFSET;
		
		for ( int i=0 ; i < content.length ; i++) {
			bytes[bytesIndex++] = content[i];
		}
	} 
	
	@Override
	public byte[] getContent() {
		// remove hash value from content
		return Arrays.copyOfRange(bytes, MAGIC_NUMBER_SIZE + HASH_OFFSET, bytes.length);
	}
	
	@Override
	public void setBytes(byte[] bytes) {
		// update hash value
		byte[] hashValueB = Arrays.copyOfRange(bytes, MAGIC_NUMBER_SIZE, HASH_OFFSET+MAGIC_NUMBER_SIZE);
		hashValue = ByteBuffer.wrap(hashValueB).getLong();
		
		this.bytes = bytes;
	}
	
	@Override
	public void setHash(long hashValue) {
		super.setHash(hashValue);
		
		byte[] hashValueB = ByteBuffer.allocate(HASH_OFFSET).putLong(hashValue).array();
		for (int i=0; i<hashValueB.length; i++)
			bytes[i+1] = hashValueB[i];
		
	}
}
