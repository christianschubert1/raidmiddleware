package cloud.content;

import cloud.interfaces.ICloudBaseHandler;
import cloud.recovery.RecoveryUtils;

public class RaidContent {
	protected Long hashValue;
	protected byte[] bytes;
	public ICloudBaseHandler handler;
	
	public final static byte RAID1_MAGIC_NUMBER = 0;
	public final static byte RAID5_MAGIC_NUMBER = 5;
	public final static int MAGIC_NUMBER_SIZE = 1;
	
	public RaidContent() {
		
	}
	
	public RaidContent(ICloudBaseHandler handler) {
		this.handler = handler;
	}
	
	public void setHash(long hashValue) {
		this.hashValue = hashValue;
	}
	
	public long getHash() {
		if (hashValue == null) {
			hashValue = RecoveryUtils.computeCRCChecksum(bytes);
		}
		
		return hashValue;
	}
	
	public void setContent(byte[] content) {
		this.bytes = content;
	}
	
	public byte[] getContent() {
		return bytes;
	}
	
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
	
	public byte[] getBytes() {
		return bytes;
	}
	
	public ICloudBaseHandler getHandler() {
		return handler;
	}
	
}
