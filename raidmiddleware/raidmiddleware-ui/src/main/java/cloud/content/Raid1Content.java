package cloud.content;

import java.util.Arrays;

import cloud.interfaces.ICloudBaseHandler;


// magic number + content
public class Raid1Content extends RaidContent {

	public Raid1Content(ICloudBaseHandler handler) {
		super(handler);
	}

	@Override
	public void setContent(byte[] content) {
		bytes = new byte[content.length + MAGIC_NUMBER_SIZE];
		bytes[0] = RAID1_MAGIC_NUMBER;
		
		for (int i=1; i < bytes.length;i++)
			bytes[i] = content[i-1];
	}
	
	@Override
	public byte[] getContent() {
		return Arrays.copyOfRange(bytes, 1, bytes.length);
	}

}
