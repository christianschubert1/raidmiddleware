package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MySQLConnection {
	
	private Connection connection;
	private String user = "raidmiddleware";
	private String pw = "aic2014";
	private String db = "raidmiddleware";
	
	private void connectToDB() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		connection = DriverManager.getConnection("jdbc:mysql://localhost/"+ db + "?user="+ user +"&password="+ pw);
	}
	
	private void disconnectFromDB() {
		if(connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {  }
		}
	}
	
	private boolean checkConnection() {
		try {
			if(connection == null || connection.isClosed()) {
				connectToDB();
			}
		} catch (SQLException | ClassNotFoundException e) {
			return false;
		}
		return true;
	}
	
	private int getIdFromPath(String path) throws SQLException {
	  int id = 0;
		
	  PreparedStatement stCheckIfExists = connection.prepareStatement("SELECT id FROM files WHERE file_path=?");
	  stCheckIfExists.setString(1, path);
	  ResultSet setExists = stCheckIfExists.executeQuery();
	  if(setExists.next()) {
	    id = setExists.getInt(1);
	  }
	  setExists.close();
	  stCheckIfExists.close();		
	  return id;
	}
	
//	private int getDeletionIdFromPath(String path) throws SQLException {
//		  int id = 0;
//		  
//		  PreparedStatement stCheckIfExists = connection.prepareStatement("SELECT id FROM pending_delete WHERE path=?");
//		  stCheckIfExists.setString(1, path);
//		  ResultSet setExists = stCheckIfExists.executeQuery();
//		  if(setExists.next()) {
//		    id = setExists.getInt(1);
//		  }
//		  setExists.close();
//		  stCheckIfExists.close();		
//		  return id;
//		}
	
	public boolean testDB() {
		try {
			connectToDB();
			disconnectFromDB();
		} catch (ClassNotFoundException | SQLException e) {
			return false;
		}
		return true;
	}
	
	public List<Integer> getAllVersions(String path) throws SQLException {
      if(!checkConnection()) return new ArrayList<>();
      
      int fileId = getIdFromPath(path);
      List<Integer> versionList = new ArrayList<>();
      
      if(fileId != 0) {
        PreparedStatement pstAllVersions = connection.prepareStatement("SELECT version FROM versions WHERE file_id=?");
        pstAllVersions.setInt(1, fileId);
        ResultSet setHighest = pstAllVersions.executeQuery();
        while(setHighest.next()) {
          versionList.add(setHighest.getInt(1));
        }
        setHighest.close();
        pstAllVersions.close();
      }

      disconnectFromDB();     
      return versionList;
    }
	
	public int getHighestVersion(String path) throws SQLException {
		int version = 0;
		if(!checkConnection()) return version;
		
		int fileId = getIdFromPath(path);
		
		if(fileId != 0) {
			PreparedStatement stHighestVersion = connection.prepareStatement("SELECT MAX(version) FROM versions WHERE file_id=?");
			stHighestVersion.setInt(1, fileId);
			ResultSet setHighest = stHighestVersion.executeQuery();
			if(setHighest.next()) {
				version = setHighest.getInt(1);
			}
			setHighest.close();
			stHighestVersion.close();
		}

		disconnectFromDB();
		
		return version;
	}
	
	public boolean fileExists(String path) throws SQLException {
		if(!checkConnection()) return false;
		int fileId = getIdFromPath(path);
		disconnectFromDB();
		
		return fileId == 0 ? false : true;
	}
	
	public void addOrUpdateFile(String path, byte[] bytes, int raidMode) throws SQLException {
		if(!checkConnection()) return;
		
		int maxVersion = 0;
		int fileId = getIdFromPath(path);
		
		if(fileId != 0) {
			PreparedStatement stHighestVersion = connection.prepareStatement("SELECT MAX(version) FROM versions WHERE file_id=?");
			stHighestVersion.setInt(1, fileId);
			ResultSet setHighest = stHighestVersion.executeQuery();
			if(setHighest.next()) {
				maxVersion = setHighest.getInt(1);
			}
			setHighest.close();
			stHighestVersion.close();
			
			// update raid mode
			PreparedStatement stUpdateRaidMode = connection.prepareStatement("UPDATE files SET raid_mode=? WHERE id=?");
			stUpdateRaidMode.setInt(1, raidMode);
			stUpdateRaidMode.setInt(2, fileId);
			stUpdateRaidMode.executeUpdate();
			stUpdateRaidMode.close();
			
		}
		else {
			PreparedStatement stInsertNewFile1 = connection.prepareStatement("INSERT INTO files(file_path, raid_mode) VALUES (?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
			stInsertNewFile1.setString(1, path);
			stInsertNewFile1.setInt(2, raidMode);
			stInsertNewFile1.executeUpdate();
			ResultSet setKeys = stInsertNewFile1.getGeneratedKeys();
			if(setKeys != null && setKeys.next()) {
				fileId = (int) setKeys.getLong(1);
			}
			stInsertNewFile1.close();
		}
		
		PreparedStatement stInsertNewFile2 = connection.prepareStatement("INSERT INTO versions(file_id, version, file_blob) VALUES (?,?,?)");
		stInsertNewFile2.setInt(1, fileId);
		stInsertNewFile2.setInt(2, maxVersion + 1);
		stInsertNewFile2.setBytes(3, bytes);
		stInsertNewFile2.executeUpdate();
		stInsertNewFile2.close();
		
		disconnectFromDB();
	}
	
	public void deleteFolder(String folderPath) throws SQLException {
	  if(!checkConnection()) return;
		
	  ArrayList<Integer> idList = new ArrayList<Integer>();
		
	  Statement stIds = connection.createStatement();
	  ResultSet setIds = stIds.executeQuery("SELECT id FROM files WHERE file_path LIKE '"+ folderPath +"%'");
	  while(setIds.next()) {
	    idList.add(setIds.getInt(1));
	  }
	  setIds.close();
	  stIds.close();		
	  
	  if (idList.isEmpty()) {
        return;
      }
	  
	  String ids = "";
	  
	  for (Integer i : idList) {
	  	ids += i;
	  	ids += ",";
	  }
	  
	  Statement stDeleteFiles = connection.createStatement();
	  stDeleteFiles.executeUpdate("DELETE FROM files WHERE id IN (" + ids.substring(0, ids.length() - 1) + ")");
	  stDeleteFiles.close();
	  
	  Statement stDeleteVersions = connection.createStatement();
	  stDeleteVersions.executeUpdate("DELETE FROM versions WHERE file_id IN (" + ids.substring(0, ids.length() - 1) + ")");
	  stDeleteVersions.close();
		
		disconnectFromDB();
	}
	
	public void deleteFile(String path) throws SQLException {
		if(!checkConnection()) return;
		
		int fileId = getIdFromPath(path);

		if(fileId != 0) {
			PreparedStatement stDeleteFile1 = connection.prepareStatement("DELETE FROM files WHERE id=?");
			stDeleteFile1.setInt(1, fileId);
			stDeleteFile1.executeUpdate();
			stDeleteFile1.close();
			
			PreparedStatement stDeleteFile2 = connection.prepareStatement("DELETE FROM versions WHERE file_id=?");
			stDeleteFile2.setInt(1, fileId);
			stDeleteFile2.executeUpdate();
			stDeleteFile2.close();
		}
		
		disconnectFromDB();
	}
	
	public byte[] getFile(String path, int version) throws SQLException {
		
		byte[] bytes = null;
		if(!checkConnection()) return bytes;
		
		int fileId = getIdFromPath(path);
		if(fileId != 0) {
			PreparedStatement stGetBlob = connection.prepareStatement("SELECT file_blob FROM versions WHERE file_id=? AND version=?");
			stGetBlob.setInt(1, fileId);
			stGetBlob.setInt(2, version);
			ResultSet setBlob = stGetBlob.executeQuery();
			if(setBlob.next()) {
				bytes = setBlob.getBytes(1);
			}
			setBlob.close();
			stGetBlob.close();
		}
		
		disconnectFromDB();
		return bytes;
	}
	
	public int getRaidMode(String path) throws SQLException {
		int raidMode = -1;
		if(!checkConnection()) return raidMode;
		
		int fileId = getIdFromPath(path);
		if(fileId != 0) {
			PreparedStatement stGetMode = connection.prepareStatement("SELECT raid_mode FROM files WHERE id=?");
			stGetMode.setInt(1, fileId);
			ResultSet setMode = stGetMode.executeQuery();
			if(setMode.next()) {
				raidMode = setMode.getInt(1);
			}
			setMode.close();
			stGetMode.close();
		}
		
		disconnectFromDB();
		return raidMode;
	}
	
	public void addPendingDelete(String path, int handlerID) {
		if(!checkConnection()) return;
		
//		int id = getDeletionIdFromPath(path);
				
		try {
			PreparedStatement stInsertNewFile1 = connection.prepareStatement("INSERT INTO pending_delete(handler_id, path) VALUES (?,?)");
			stInsertNewFile1.setInt(1, handlerID);
			stInsertNewFile1.setString(2, path);
			stInsertNewFile1.executeUpdate();	
			stInsertNewFile1.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		disconnectFromDB();
	}
	
	public Map<Integer, List<String>> getAllPendingDeleteEntries() {
		Map<Integer,List<String>> pendingEntries = new HashMap<Integer, List<String>>();
		if(!checkConnection()) return pendingEntries;
		
		
		PreparedStatement stGetPending;
		try {
			stGetPending = connection.prepareStatement("SELECT handler_id, path FROM pending_delete");
			ResultSet setPending = stGetPending.executeQuery();
			if(setPending.next()) {
				int handler_id = setPending.getInt(1);
				String path = setPending.getString(2);
				
				List<String> paths = pendingEntries.get(handler_id);
				if (paths == null) {
					pendingEntries.put(handler_id, new ArrayList<String>());
					paths = pendingEntries.get(handler_id);
				}
				
				paths.add(path);				
			}
			setPending.close();
			stGetPending.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		disconnectFromDB();
		return pendingEntries;
	}
	
	public void removePendingDelete(int handlerID) throws SQLException {
		if(!checkConnection()) return;
		
		PreparedStatement stDelete = connection.prepareStatement("DELETE FROM pending_delete WHERE handler_id=?");
		stDelete.setInt(1, handlerID);
		stDelete.executeUpdate();
		stDelete.close();
			
		disconnectFromDB();
	}
}