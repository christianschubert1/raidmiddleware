package test;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import cloud.content.RaidContent;
import cloud.interfaces.ICloudBaseHandler;

@SuppressWarnings("javadoc")
public class TestHandler {
	
	public static void test(final ICloudBaseHandler handler) {
		Logger logger = Logger.getLogger(TestHandler.class);		
		
		logger.info("TEST STARTED WITH: " + handler.getClass().getName());
		long timestamp = System.currentTimeMillis();
		
		try {
			logger.info("started to initialize cloud handler");
			handler.init();
			logger.info("cloud handler initialized successfully");
			
			logger.info("look if folder 'Test' already exist");
			if(handler.getEntryInfo("Test/") != null) {
				logger.info("Folder 'Test' exist. Delete it.");
				
				handler.delete("Test/");
			} 
			else
				logger.info("folder 'Test' not found");
			
			logger.info("create folder 'Test'");
			handler.create("Test/", null);
			
			logger.assertLog(handler.getEntryInfo("Test/") != null, "Could not find folder 'Test'");
			
			logger.info("create file 'file.txt' in folder 'Test'");
			RaidContent raidContent = new RaidContent();
			raidContent.setContent("CREATED".getBytes());
			handler.create("Test/file.txt", raidContent);
			
			logger.assertLog(handler.getEntryInfo("Test/file.txt") != null, "Could not find file 'file.txt' in folder 'Test'");
			
			logger.info("read file 'file.txt' in folder 'Test' (option1)");
			byte[] content1 = handler.read("Test/file.txt", null).getContent();
			logger.assertLog(content1 != null, "Could not read file 'file.txt' in folder 'Test' (option1)");
			
			InputStream in = null;
			byte[] content2 = null;
			try {
				logger.info("read file 'file.txt' in folder 'Test' (option2)");
				in = handler.startRead("Test/file.txt", null);
				content2 = IOUtils.toByteArray(in);
				
				logger.assertLog(content2 != null, "Could not read file 'file.txt' in folder 'Test' (option2)");
				logger.assertLog(Arrays.equals(content1, content2), "(read) content not equal.");
			} catch (Exception e) {
				logger.error("could not read file 'file.txt' in folder 'Test' (option2)");
			} finally {
				if(in != null)
					in.close();
			}

			logger.info("update file 'file.txt' in folder 'Test'");
			raidContent.setContent((new String(content1) + " -> UPDATED").getBytes());
			handler.update("Test/file.txt", raidContent);
			
			logger.info("create folder 'SubFolder' in folder 'Test'");
			handler.create("Test/SubFolder/", null);
			
			logger.assertLog(handler.getEntryInfo("Test/SubFolder/") != null, "Could not find folder 'Test/SubFolder/'");
			
			logger.info("START: create file 'large.txt' in folder 'Test/SubFolder/'");
			Path sourceFilePath = Paths.get("src", "test", "resources", "large.txt");
			raidContent.setContent(Files.readAllBytes(sourceFilePath));
			handler.create("Test/SubFolder/large.txt", raidContent);
			logger.info("STOP:created file 'large.txt' in folder 'Test/SubFolder/'");
			
			logger.assertLog(handler.getEntryInfo("Test/SubFolder/") != null, "Could not find file 'Test/SubFolder/SubFile.txt'");
					
			logger.info("delete the folder 'Test'");
			handler.delete("Test/");

			logger.info("time elapsed: " + (System.currentTimeMillis() - timestamp)/1000.0 + "s");				
		} catch (Exception e) {
			try {
				logger.info("delete the folder 'Test'");
				handler.delete("Test/");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			logger.error(e.getMessage());
		}	
		logger.info("TEST END: " + handler.getClass().getName());	
	}
}
