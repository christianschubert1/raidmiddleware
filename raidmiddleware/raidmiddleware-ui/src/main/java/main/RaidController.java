package main;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import utils.Raid5Utils;
import cloud.amazon.AmazonS3Handler;
import cloud.box.BoxHandler;
import cloud.content.Raid1Content;
import cloud.content.Raid5Fragment;
import cloud.content.RaidContent;
import cloud.dropbox.DropBoxHandler;
import cloud.entry.EntryWrapper;
import cloud.interfaces.ICloudBaseHandler;
import cloud.interfaces.IRaidController;
import cloud.recovery.RecoveryManager;
import cloud.recovery.RecoveryUtils;
import db.MySQLConnection;

public class RaidController implements IRaidController {
  private static final Logger logger = LogManager.getLogger(RaidController.class.getName());
	
  private DropBoxHandler dropBoxHandler;
  private BoxHandler boxHandler;
  private AmazonS3Handler amazonHandler;
  
  private final MySQLConnection sqlConn;
  
  private Path currentPath = Paths.get("");
  
  public Path getCurrentPath() {
    return currentPath;
  }

  public void setCurrentPath(Path currentPath) {
    this.currentPath = currentPath;
  }

  private int MODE = 0;
  public int getMODE() {
    return MODE;
  }

  private final List<ICloudBaseHandler> handlerList = new ArrayList<>();
  public List<ICloudBaseHandler> getHandlerList() {
    return handlerList;
  }

  private final Map<Integer, ICloudBaseHandler> handlerOrderedList = new HashMap<Integer, ICloudBaseHandler>();
  private final ExecutorService pool = Executors.newCachedThreadPool();
  
  private final static int AMAZON_LIST_INDEX = 0;
  private final static int DROPBOX_LIST_INDEX = 1;
  private final static int BOX_LIST_INDEX = 2;
	
  private final static int[] RAID5_DISK_ORDER = {AMAZON_LIST_INDEX, DROPBOX_LIST_INDEX, BOX_LIST_INDEX};	
	
  public RaidController() {		
		
    sqlConn = new MySQLConnection();
		  
    //BOX API: VERY SLOW
    try {
      boxHandler = new BoxHandler(); //auto INIT
      handlerList.add(boxHandler);
      handlerOrderedList.put(BOX_LIST_INDEX, boxHandler);
    } catch (Exception e) {
      logger.error("Box is not available yet.", e);
    }
    
    //DROPBOX API: LESS SLOWER THAN AMAZON S3
    try {
      dropBoxHandler = new DropBoxHandler(); //auto INIT
      handlerList.add(dropBoxHandler);
      handlerOrderedList.put(DROPBOX_LIST_INDEX, dropBoxHandler);
    } catch (Exception e) {
      logger.error("Dropbox is not available yet.", e);
    }

    //AMAZON S3 API: FASTEST API
    try {
      amazonHandler = new AmazonS3Handler();
      handlerList.add(amazonHandler);
      handlerOrderedList.put(AMAZON_LIST_INDEX, amazonHandler);
    } catch (Exception e) {
      logger.error("Amazon is not available yet.", e);
    }

    
			
    switch (MODE = handlerList.size()) {
      case 0:	logger.error("Currently no cloud storage available.");
      return;
      case 1:	logger.info("Currently only ONE cloud storage available.");
      logger.info("Recovery Mode off");
      logger.info("Raid 5 off");
      logger.info("Available storage: " + handlerList.get(0).getID());
      break;	
      case 2: logger.info("Currently only TWO cloud storages available.");
      logger.info("Recovery Mode on");
      logger.info("Raid 5 Mode on");
      logger.info("Available storages: " + handlerList.get(0).getID() + ", " + handlerList.get(1).getID());
      break;				
      case 3:	logger.info("ALL cloud storages available :)");
      logger.info("Recovery Mode on");
      logger.info("Raid 5 Mode on");
      break;			
      default:return;
    }
    
    // execute pending deletion tasks
  	Map<Integer, List<String>> pendingDelete = sqlConn.getAllPendingDeleteEntries();
  	
  	for (int handlerId : pendingDelete.keySet()) {
  		ICloudBaseHandler handler = handlerOrderedList.get(handlerId);
  		if (handler != null) {
  			for (String path : pendingDelete.get(handlerId)) {
	 				try {
	 					handler.delete(path);
	 					sqlConn.removePendingDelete(handlerId);
	 				} catch (Exception e) {
	 					logger.error(handler.getID() + ": " + e.getMessage(), e);
	 				}
	  		}
  		}
  	}
    
//			try {
//				scanInput();
//			} catch (IOException e) {
//				logger.error("unexpected error", e.getCause());
//			}		
	}	
	
//	private void scanInput() throws IOException {
//		// Test of Controller logic (mirroring (creating and updating), deleting and reading from all storages)				
//		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
//		String line;
//		System.out.print("cloud-root/" + currentPath + ">");
//		
//		try {
//			while ((line = br.readLine()) != null) {		
//				String[] args = line.split("\\s+");
//								
//				if(args.length != 0) {
//					switch (args[0]) {
//						case "create": {
//							if(args.length < 2 || args.length > 3) {
//								logger.error("Input error. Type \"help\" for more information.");
//								break;
//							}
//							boolean raid5 = false;
//							String filename = args[1];
//							if (args.length == 3) {
//								raid5 = (args[1].equals("raid5")) ? true : false;
//								filename = args[2];
//							}
//							
//							Path sourceFilePath = Paths.get("src", "test", "resources", filename);
//							String cloudPathString = getCloudPath(filename);
//						
//							try {
//								byte[] bytes = Files.readAllBytes(sourceFilePath);								
//								if (raid5) {
//									if (MODE >= 2)
//										createRaid5Entry(cloudPathString, bytes);
//									else
//										logger.error("Raid 5 works only with at least two cloud storages available!");
//								}
//								else
//									createRaid1Entry(cloudPathString, bytes);
//							} catch (IOException e) {
//								logger.error("Error while reading file '" + sourceFilePath + "'");
//							}
//							break;
//						}
//						case "update": {
//							if(args.length < 2 || args.length > 3) {
//								logger.error("Input error. Type \"help\" for more information.");
//								break;
//							}
//							
//							boolean raid5 = false;
//							String filename = args[1];
//							
//							if (args.length == 3) {
//								raid5 = (args[1].equals("raid5")) ? true : false;
//								filename = args[2];
//							}
//							
//							Path sourceFilePath = Paths.get("src", "test", "resources", filename);
//							String cloudPathString = getCloudPath(filename);
//							
//							try {
//								byte[] bytes = Files.readAllBytes(sourceFilePath);
//								if (raid5)
//									if (MODE >= 2)
//										updateRaid5Entry(cloudPathString, bytes);
//									else
//										logger.error("Raid 5 works only with at least two cloud storages available!");									
//								else
//									updateRaid1Entry(cloudPathString, bytes);
//							} catch (IOException e) {
//								logger.error("Error while reading file '" + sourceFilePath + "'");
//							}
//							break;
//						}
//						case "createdir": {
//							if(args.length != 2) {
//								logger.error("Input error. Type \"help\" for more information.");
//								break;
//							}
//							
//							String cloudPathString = getCloudPath(args[1]);
//							createDirectory(cloudPathString + "/");
//							break;
//						}
//						case "read": {
//							if(args.length != 2) {
//								logger.error("Input error. Type \"help\" for more information.");
//								break;
//							}
//							
//							String cloudPathString = getCloudPath(args[1]);
//							try {
//								byte[] content = readEntry(cloudPathString);
//																
//								if (content != null) {
//									Path newFilePath = Paths.get("src", "test", "downloaded", args[1]);
//									Files.write(newFilePath, content);
//								}
//							} catch (Exception e) {
//								logger.error("Error while downloading file '" + cloudPathString + "'", e);
//							}
//							break;
//						}
//						case "delete": {
//							if(args.length != 2) {
//								logger.error("Input error. Type \"help\" for more information.");
//								break;
//							}
//
//							String cloudPathString = getCloudPath(args[1]);
//							cloudPathString = args[1].endsWith("/") ? cloudPathString+"/" : cloudPathString;							
//							deleteEntry(cloudPathString);
//							break;
//						}
//						case "ls": {
//							boolean detailedInfo = false;
//							if (args.length == 2 && args[1].equals("-a"))
//								detailedInfo = true;
//							listDirectory(getCloudPath(null), detailedInfo);
//							break;
//						}
//						case "cd": {
//							if(args.length != 2) {
//								logger.error("Input error. Type \"help\" for more information.");
//								break;
//							}
//							
//							if(args[1].equals("..")) {
//								if((currentPath = navigateBack()) == null)
//									currentPath = Paths.get("");
//							}
//							else {
//								currentPath = navigateTo(args[1]);
//							}
//							break;
//						}
//						
//						case "exit":
//							return;
//							
//						default: 
//							logger.info("Available options: ");
//							logger.info(">> " + "create [raid5] <file>" + "\t" + "Uploads the given file to all storage. If \"raid5\" option is present a Raid 5 file will be created. Default is Raid 0.");
//							logger.info(">> " + "createdir <name>" + "\t" + "Creates a new directory with the given name");
//							logger.info(">> " + "update [raid5] <file>" + "\t" + "Replace the given file on all storages. Use \"raid5\" option for updating Raid 5 files, otherwise Raid 0 will be used.");
//							logger.info(">> " + "delete <file>" + "\t" + "Deletes the given file from all storages");
//							logger.info(">> " + "read <file>" + "\t\t" + "Downloads the given file to bin/resources");
//							logger.info(">> " + "ls [-a]" + "\t\t\t" + "List all files/folder in the current directory. Use option \"-a\" display all information.");
//							logger.info(">> " + "cd <dir>" + "\t\t" + "Navigate through folders");
//							logger.info(">> " + "exit" + "\t\t\t" + "Quit the program");
//							break;
//					}	
//				}
//				System.out.print("cloud-root/" + FilenameUtils.separatorsToUnix(currentPath.toString()) + ">");
//			}	
//		}
//		finally {			
//			br.close();
//			pool.shutdown();
//			pool.shutdownNow();
//			logger.info("shutdown successful");
//		}		
//	}
	
//	public void uploadFile() {
//	  String cloudPathString = getCloudPath(filename);
//    
//        try {
//            byte[] bytes = Files.readAllBytes(sourceFilePath);                              
//            if (raid5) {
//                if (MODE >= 2)
//                    createRaid5Entry(cloudPathString, bytes);
//                else
//                    logger.error("Raid 5 works only with at least two cloud storages available!");
//            }
//            else
//                createRaid1Entry(cloudPathString, bytes);
//        } catch (IOException e) {
//            logger.error("Error while reading file '" + sourceFilePath + "'");
//        }
//	}
  
  public void restoreToVersion(String filename, int version) {
    try {
      byte[] bytes = sqlConn.getFile(getCloudPath(filename), version);
      int raidMode = sqlConn.getRaidMode(filename);
//      sqlConn.addOrUpdateFile(filename, bytes);
      uploadFile(filename, bytes, raidMode);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
	
	public void createFolder(String foldername) {
	  createDirectory(getCloudPath(foldername));
	}
	
	public byte[] downloadFile(String filename) {
	  return readEntry(getCloudPath(filename));
	}
	
	public void delete(String name) {
	  String cloudPathString = getCloudPath(name);
      cloudPathString = name.endsWith("/") ? cloudPathString+"/" : cloudPathString;                            
      deleteEntry(cloudPathString);
      try {
      	if(cloudPathString.endsWith("/")) {
      		sqlConn.deleteFolder(cloudPathString);
      	}
      	else {
      		sqlConn.deleteFile(cloudPathString);
				}
      } catch (SQLException e) {
        e.printStackTrace();
      }
	}
	
	public void uploadFile(String filename, byte[] bytes, int raidMode) {
	  if (raidMode == EntryWrapper.RAID_5_MODE)
        createRaid5Entry(getCloudPath(filename), bytes);
	  else if (raidMode == EntryWrapper.RAID_1_MODE)
	    createRaid1Entry(getCloudPath(filename), bytes);
	  else
		  logger.error("Unknown raid mode!");
	  try {
	    sqlConn.addOrUpdateFile(getCloudPath(filename), bytes, raidMode);
	  } catch (SQLException e) {
	    e.printStackTrace();
      }
	}
	
	public Map<String, List<EntryWrapper>> ls(String path, boolean detailedInfo) {
	  return getAllEntriesIn(getCloudPath(null), detailedInfo);
	}
	
	public void cd(String foldername) {  
	  if(foldername.equals("..")) {
	    if((currentPath = navigateBack()) == null)
	      currentPath = Paths.get("");
	  }
	  else {
	    currentPath = navigateTo(foldername);
	  }
	}
	
	public String getCloudPath(final String name) {
	  if(name == null || name.isEmpty())
	    return FilenameUtils.separatorsToUnix(currentPath.toString());
	  return FilenameUtils.separatorsToUnix(currentPath.resolve(name).toString());
	}
	
//	public static void main(String[] args) {
//		BasicConfigurator.configure();
//		LogManager.getRootLogger().setLevel(Level.INFO);
//		new RaidController();
//	}

	public void createDirectory(final String path) {		
	  Set<Callable<Void>> callables = new HashSet<Callable<Void>>();
		
	  for (final ICloudBaseHandler h : handlerList) {
	    callables.add(new Callable<Void>() {			
	      @Override
	      public Void call() {
	        try {
	          h.create(path, null);
	        } 
	        catch (Exception e) {
	          logger.error(h.getID() + ": " + e.getMessage(), e);
	        }	
	        return null;
	      }
	    });
	  }
		
	  try {
	    pool.invokeAll(callables);
	  } catch (InterruptedException e) {
	    logger.error(e.getMessage(), e);
	  } 
	}
	
	@Override
	public void createRaid1Entry(final String path, final byte[] content) {
		Set<Callable<Void>> callables = new HashSet<Callable<Void>>();
		
		for (final ICloudBaseHandler h : handlerList) {
			callables.add(new Callable<Void>() {			
				@Override
				public Void call() {
					try {
						RaidContent raidContent = new Raid1Content(h);
						raidContent.setContent(content);
						h.create(path, raidContent);
					} 
					catch (Exception e) {
		      	logger.error(h.getID() + ": " + e.getMessage(), e);
		      }	
					return null;
				}
			});
		}
		
		try {
			pool.invokeAll(callables);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		} 
	}
	
	@Override
	public void createRaid5Entry(final String path, byte[] content) {
		Set<Callable<Void>> callables = new HashSet<Callable<Void>>();
		final byte[][] contentRaid5 = Raid5Utils.getRaid5Fragments(content);
		final long hashValue = RecoveryUtils.computeCRCChecksum(content);
		
		for (int i=0; i < 3; i++) {
			final ICloudBaseHandler h = handlerOrderedList.get(RAID5_DISK_ORDER[i]);
			if (h == null)
				continue;
			final int index = i;
			callables.add(new Callable<Void>() {			
				@Override
				public Void call() {
					try {
						Raid5Fragment raid5Frag = new Raid5Fragment(h);
						raid5Frag.setContent(contentRaid5[index]);
						raid5Frag.setHash(hashValue); 		// set hash of file (not of fragment)
						h.create(path, raid5Frag);
					} 
					catch (Exception e) {
		      	logger.error(h.getID() + ": " + e.getMessage(), e);
		      }	
					return null;
				}
			});
		}
		
		try {
			pool.invokeAll(callables);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		} 
	}
	
	private int getOldRaidMode(String path) {
		int oldRaidMode = EntryWrapper.RAID_UNDEFINED_MODE;
		for (int i=0; i<3; i++) {
			 ICloudBaseHandler h = handlerOrderedList.get(RAID5_DISK_ORDER[i]);
			 if (h != null) {
				 try {
					oldRaidMode = h.getRaidMode(path, null);
					break;
				} catch (Exception e) {
					e.printStackTrace();
				}
			 }
		}
		return oldRaidMode;
	}
	
	@Override
	public void updateRaid5Entry(final String path, byte[] content) {
		
		// allow raid mode change only if all clouds available
		if (getOldRaidMode(path) != EntryWrapper.RAID_5_MODE && MODE < 3) {
			logger.error("You can't store the data as Raid 5, because the old data is stored as Raid 0 and not all storage providers are available!");
		}
		
		Set<Callable<Void>> callables = new HashSet<Callable<Void>>();
		final byte[][] contentRaid5 = Raid5Utils.getRaid5Fragments(content);
		for (int i=0; i < 3; i++) {
			final ICloudBaseHandler h = handlerOrderedList.get(RAID5_DISK_ORDER[i]);
			if (h == null)
				continue;
			final int index = i;
			callables.add(new Callable<Void>() {			
				@Override
				public Void call() {
					try {
						Raid5Fragment raid5Frag = new Raid5Fragment(h);
						raid5Frag.setContent(contentRaid5[index]);
						h.update(path, raid5Frag);
					} 
					catch (Exception e) {
		      	logger.error(h.getID() + ": " + e.getMessage(), e);
		      }	
					return null;
				}
			});
		}
		
		try {
			pool.invokeAll(callables);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		} 
	}

	@Override
	public void updateRaid1Entry(final String path, final byte[] content) {
		
		// allow raid mode change only if all clouds available
			if (getOldRaidMode(path) != EntryWrapper.RAID_1_MODE && MODE < 3) {
				logger.error("You can't store the data as Raid 0, because the old data is stored as Raid 5 and not all storage providers are available!");
			}
			
		Set<Callable<Void>> callables = new HashSet<Callable<Void>>();
		
		for (final ICloudBaseHandler h : handlerList) {
			callables.add(new Callable<Void>() {			
				@Override
				public Void call() {
					try {
						RaidContent raidContent = new Raid1Content(h); 
						raidContent.setContent(content);
						h.update(path, raidContent);
					} 
					catch (Exception e) {
		      	logger.error(h.getID() + ": " + e.getMessage(), e);
		      }	
					return null;
				}
			});
		}
		
		try {
			pool.invokeAll(callables);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		} 
	}
	
	@Override
	public byte[] readEntry(String path) {
		byte[] content = null;		
		logger.info("'" + path +  "'");
		
		
		ICloudBaseHandler h1 = handlerOrderedList.get(RAID5_DISK_ORDER[0]);
		ICloudBaseHandler h2 = handlerOrderedList.get(RAID5_DISK_ORDER[1]);
		ICloudBaseHandler h3 = handlerOrderedList.get(RAID5_DISK_ORDER[2]);
		
		RaidContent h1Content = null;
		RaidContent h2Content = null;
		RaidContent h3Content = null;
		
		
		
		try {
		  int raidMode = sqlConn.getRaidMode(path);
			
		  if(h1 != null) {
		    if((h1Content = h1.read(path, null)) == null)
		      h1Content = raidMode == EntryWrapper.RAID_5_MODE ? new Raid5Fragment(h1) : new Raid1Content(h1);
		  }
		  if(h2 != null) {
		    if((h2Content = h2.read(path, null)) == null)
		      h2Content = raidMode == EntryWrapper.RAID_5_MODE ? new Raid5Fragment(h2) : new Raid1Content(h2);
		  }
		  if(h3 != null) {
		    if((h3Content = h3.read(path, null)) == null)
		      h3Content = raidMode == EntryWrapper.RAID_5_MODE ? new Raid5Fragment(h3) : new Raid1Content(h3);
		  }
  		}
		catch (Exception e) {
          e.printStackTrace();
        }
		
		
//		try {
//			if (h1.getEntryInfo(path) != null)
//				h1Content = h1 != null ? h1.read(path, null) : new Raid5Fragment(h1);
//			if (h2.getEntryInfo(path) != null)
//				h2Content = h2 != null ? h2.read(path, null) : new Raid5Fragment(h2);
//			if (h3.getEntryInfo(path) != null)
//				h3Content = h3 != null ? h3.read(path, null) : new Raid5Fragment(h3);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		List<RaidContent> raidContentsNotNull = new ArrayList<RaidContent>();
		raidContentsNotNull.add(h1Content); raidContentsNotNull.add(h2Content); raidContentsNotNull.add(h3Content);
		raidContentsNotNull.removeAll(Collections.singleton(null));
		RaidContent correctedContent = null;
		// recover if neccessary
		if (MODE > 1 && ! RecoveryManager.getInstance().recover(raidContentsNotNull, path)) {
		  // recovery failed
		  logger.error("Reading failed! Recovery failed! All hash values differ, Files are missing or read error occured.");
		  return null;
		} else if ((correctedContent = RecoveryManager.getInstance().getFaultyRaidContent()) != null) {
			int id = RecoveryManager.getInstance().getFaultyID();
			
			if (id == RAID5_DISK_ORDER[0])
				h1Content = correctedContent;
			else if (id == RAID5_DISK_ORDER[1])
				h2Content = correctedContent;
			else
				h3Content = correctedContent;
		}
		
		// raid 5
		if (h1Content instanceof Raid5Fragment || h2Content instanceof Raid5Fragment || h3Content instanceof Raid5Fragment) {
			if (MODE >= 2) {
				byte[] h1Bytes = h1Content != null ? h1Content.getContent() : null;
				byte[] h2Bytes = h2Content != null ? h2Content.getContent() : null;
				byte[] h3Bytes = h3Content != null ? h3Content.getContent() : null;
				
				// check that at least two raid 5 fragments are not null				
				if (raidContentsNotNull.size() < 2) {
					logger.error("Raid 5 content could not be read. Needs at least two valid Raid 5 files available.");
					return null;
				}
								
				content = Raid5Utils.getContentFromRaid5Files(h1Bytes, h2Bytes, h3Bytes);
			} else {
				logger.error("Raid 5 works only with at least two cloud storages available!");
				return null;
			}
		// raid 1
		} else if (raidContentsNotNull.size() > 0) {
			content = raidContentsNotNull.get(0).getContent();
		}			
		
		return trim(content);
	}
	
	private byte[] trim(byte[] bytes)
	{
	    int i = bytes.length - 1;
	    while (i >= 0 && bytes[i] == 0)
	    {
	        --i;
	    }

	    return Arrays.copyOf(bytes, i + 1);
	}

	@Override
	public void deleteEntry(final String path) {
	Set<Callable<Void>> callables = new HashSet<Callable<Void>>();
	
	for (int i=0; i < 3; i++) {
		final ICloudBaseHandler h = handlerOrderedList.get(RAID5_DISK_ORDER[i]);
		if (h == null) {
			// store task in db -> delete entry when cloud is available again
			sqlConn.addPendingDelete(path, i);
		} else {
			callables.add(new Callable<Void>() {			
				@Override
				public Void call() {
					try {
						h.delete(path);
					} 
					catch (Exception e) {
		      	logger.error(h.getID() + ": " + e.getMessage(), e);
		      }	
					return null;
				}
			});
		}
	}
	
	try {
		pool.invokeAll(callables);
	} catch (InterruptedException e) {
		logger.error(e.getMessage(), e);
	} 
}

	public Map<String, List<EntryWrapper>> getAllEntriesIn(final String path, final boolean detail) {
	  Set<Callable<List<EntryWrapper>>> callables = new HashSet<Callable<List<EntryWrapper>>>();
			
	  for (final ICloudBaseHandler h : handlerList) {	
	    callables.add(new Callable<List<EntryWrapper>>() {
	      @Override
	      public List<EntryWrapper> call() {
	        try {
	          return h.list(path, detail);
	        } catch (Exception e) {
	          logger.error(h.getID() + ": " + e.getMessage(), e);
	        } finally {
	          //logger.info(h.getName() + " finished");
	        }
	        return null;
	      }
	    });
	  }
			
	  Map<String, List<EntryWrapper>> mergedMap = new HashMap<String, List<EntryWrapper>>();
	  
	  try {
	    for (Future<List<EntryWrapper>> task: pool.invokeAll(callables)) {	
	      try {
	        List<EntryWrapper> list = task.get();	    	
	        if(list == null || list.isEmpty())
	          continue;
	        
	        for(EntryWrapper e: list) {		
	        	
//	          mergedMap.put(e.getFilename(), e);
//	          String raidVariant = "";						
//	          if (detail && e.isFile())
//	            raidVariant = (e.getRaidMode() == EntryWrapper.RAID_5_MODE)?" (raid5)":" (raid0)";
	          if(mergedMap.get(e.getFilename()) == null)
	            mergedMap.put(e.getFilename(), new ArrayList<EntryWrapper>());
	          mergedMap.get(e.getFilename()).add(e);
//	          mergedMap = addEntryToMap(e.getFilename(), e.getOwner());
	        }	
	      } catch (InterruptedException | ExecutionException e) {
	        e.printStackTrace();
	      }
	    }  
	  } catch (InterruptedException e) {
	    logger.error(e.getMessage(), e);
	  } 
	  
//	  System.out.println("mergedMap = " + mergedMap);
			
//	  HashMap<String, List<String>> entryMap = new HashMap<String, List<String>>();
//
//	  for(Entry<String, List<Map<String,String>>> entry : mergedMap.entrySet()) {
//	    String list = "[";
//	    for (Map<String,String> map : entry.getValue()) {
////	      System.out.println(map);
//	      for (Entry<String,String> entry2 : map.entrySet()) {
//	        list += entry2.getKey() + entry2.getValue() + ", ";
//	      }
//	    }
//	    list = list.substring(0, list.length()-2) + "]";
//	    System.out.println("\t" + entry.getKey() + " (" + list + ")");
//	  }
	  return mergedMap;
	}
	
//	private Map<String, List<Map<String,String>>> addEntryToMap(final String key, final String handlerName, String raidVariant, Map<String, List<Map<String,String>>> map) {
//		if(map.get(key) == null)
//			map.put(key, new ArrayList<Map<String,String>>());
//		
//		Map<String,String> map2 = new HashMap<String, String>();
//		map2.put(handlerName, raidVariant);
//		map.get(key).add(map2);
//		return map;
//	}

	@Override
	public Path navigateTo(String path) {
		return currentPath.resolve(path);
	}

	@Override
	public Path navigateBack() {
		return currentPath.getParent();
	}

  @Override
  public void listDirectory(String path, boolean detail) {
    // TODO Auto-generated method stub
    
  }

  public MySQLConnection getSqlConn() {
    return sqlConn;
  }
}
