package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

import utils.Raid5Utils;

/**
 * FOWARD LAYOUT
 * 
 * Disk 1   |   Disk 2   |  Disk 3
 *---------------------------------
 *   D1     |     D2     |   P1_2
 *---------------------------------
 *	 P3_4   |     D3     |    D4
 *---------------------------------
 *   D5     |   P5_6     |    D6
 *
 * D1...Data Block 1
 * P1_2... Parity Block of D1 and D2
 * 
 * for other layouts see: http://www.z-a-recovery.com/articles/raid5-variations.aspx
 */

public class Raid5Local {
	private List<File> disks; 
	private final static int BLOCK_SIZE = 2; // in bytes
	
	public Raid5Local(File disk1, File disk2, File disk3) {
		disks = new ArrayList<File>(Arrays.asList(disk1, disk2, disk3));
	}	
	
	public void writeFileVerySlow(File file, boolean update) {
		if (! update) {
			for (File disk : disks)
				disk.delete();
		}
			
		byte[] dataBlock = new byte[BLOCK_SIZE*2];
		int parityPosition = 2;
		try {	
			FileInputStream fis = new FileInputStream(file);

			while (true) {
				// clear buffer
				Arrays.fill(dataBlock, (byte) 0);
				// read 2 blocks from file
				int bytesRead = fis.read(dataBlock,0,BLOCK_SIZE*2);
				if (bytesRead == -1)
					break;
				// write the 2 blocks and parity block to disks
				write2Blocks(dataBlock, parityPosition);
				// set next parity position
				parityPosition = Raid5Utils.nextParityPosition(parityPosition);
			}
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	// write 2 blocks and parity bit to disks
	private void write2Blocks(byte[] b, int parityPosition) {
		assert (b.length == BLOCK_SIZE*2);
//		System.out.println("b1: " + (int) (b[0] & 0xFF));
//		System.out.println("b2: " + (int) (b[1] & 0xFF));
//		System.out.println("b3: " + (int) (b[2] & 0xFF));
//		System.out.println("b4: " + (int) (b[3] & 0xFF));
		try {
			List<Integer> writingPos = new ArrayList<Integer>(Arrays.asList(0,1,2));
			writingPos.remove(parityPosition);
			
			// split into block size
			byte[] block1 = Arrays.copyOfRange(b, 0, BLOCK_SIZE);
			byte[] block2 = Arrays.copyOfRange(b, BLOCK_SIZE, BLOCK_SIZE*2);
			
			// write first block to disk
			FileOutputStream fos = new FileOutputStream(disks.get(writingPos.get(0)), true);
			fos.write(block1);
			fos.close();
			
			// write second block to another disk
			fos = new FileOutputStream(disks.get(writingPos.get(1)), true);
			fos.write(block2);
			fos.close();
			
			// write parity block to remaining disk
			fos = new FileOutputStream(disks.get(parityPosition), true);
			byte[] parityBlock = Raid5Utils.computeParityBlock(block1, block2);
			fos.write(parityBlock);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public byte[] readFileSlow() {
		int parityPosition = 2;
		List<Integer> writingPosition = new ArrayList<Integer>(Arrays.asList(0,1,2));
		writingPosition.remove(parityPosition);
		
		List<Byte> readedBytes = new ArrayList<Byte>();
		byte[] block1 = new byte[BLOCK_SIZE];
		byte[] block2 = new byte[BLOCK_SIZE];
		byte[] parity = new byte[BLOCK_SIZE];
		
		FileInputStream fis1 = null;
		FileInputStream fis2 = null;
		FileInputStream fis3 = null;		
		try {
			fis1 = new FileInputStream(disks.get(0));
			fis2 = new FileInputStream(disks.get(1));
			fis3 = new FileInputStream(disks.get(2));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		while (true) {
			try {
				int bytesRead = -1;
				if (parityPosition == 0) {
					bytesRead = fis1.read(parity);
					fis2.read(block1);
					fis3.read(block2);
				} else if (parityPosition == 1) {
					bytesRead = fis1.read(block1);
					fis2.read(parity);
					fis3.read(block2);
				} else {
					bytesRead = fis1.read(block1);
					fis2.read(block2);
					fis3.read(parity);
				}
				// reached EOF
				if (bytesRead == -1)
					break;
												
				readedBytes.addAll(Arrays.asList(ArrayUtils.toObject(block1)));
				readedBytes.addAll(Arrays.asList(ArrayUtils.toObject(block2)));
				
				// set next parity position
				parityPosition = Raid5Utils.nextParityPosition(parityPosition);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			fis1.close();
			fis2.close();
			fis3.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ArrayUtils.toPrimitive(readedBytes.toArray(new Byte[readedBytes.size()]));
	}
	
	public static void main(String[] args) {
		// DISKS
		File disk1 = Paths.get("src","test","resources","disk1").toFile();
		File disk2 = Paths.get("src","test","resources","disk2").toFile();
		File disk3 = Paths.get("src","test","resources","disk3").toFile();
		// TEST FILE
		File testFile = Paths.get("src", "test", "resources", "test.txt").toFile();
		File outFile = Paths.get("src", "test", "resources", "test2.txt").toFile();
		
		long start, end;
		float durationSec;
		Raid5Local r5 = new Raid5Local(disk1, disk2, disk3);
		
//		start = System.nanoTime();
//		r5.writeFile(testFile,false);
//		end = System.nanoTime();
//		durationSec = (end-start) / 1000000000.0f;
//		System.out.println("write file: " + durationSec + " sec");
		
		start = System.nanoTime();
		r5.writeFile(testFile);
		end = System.nanoTime();
		durationSec = (end-start) / 1000000000.0f;
		System.out.println("write file: " + durationSec + " sec");
		
		byte[] content1 = null;
		byte[] content2 = null;
		byte[] content3 = null;
		
		try {
			start = System.nanoTime();
			content1 = Files.readAllBytes(disk1.toPath());
			content2 = Files.readAllBytes(disk2.toPath());
			content3 = Files.readAllBytes(disk3.toPath());
			end = System.nanoTime();
			durationSec = (end-start) / 1000000000.0f;
			System.out.println("read contents of 3 files: " + durationSec + " sec");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		start = System.nanoTime();
		byte[] fileContent = Raid5Utils.getContentFromRaid5Files(content1, content2, null);
		end = System.nanoTime();
		durationSec = (end-start) / 1000000000.0f;
		System.out.println("read file: " + durationSec + " sec");
		
		
		try {
			FileOutputStream oStream = new FileOutputStream(outFile);
			oStream.write(fileContent);
			oStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("file content size: " + fileContent.length);
	//	System.out.println("file content:" + new String(fileContent));
	}
	
	public void writeFile(File file) {
		long start = System.nanoTime();
		byte[][] frag = null;
		try {
			frag = Raid5Utils.getRaid5Fragments(Files.readAllBytes(file.toPath()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		long end = System.nanoTime();
		float durationSec = (end-start) / 1000000000.0f;
		System.out.println("fragmentsFast: " + durationSec + " sec");
		
		try {
			// write raid5 data to disk1
			FileOutputStream fos = new FileOutputStream(disks.get(0));
			fos.write(frag[0]);
			fos.close();
			
			// write raid5 data to disk2
			fos = new FileOutputStream(disks.get(1));
			fos.write(frag[1]);
			fos.close();
			
			// write raid5 data to disk3
			fos = new FileOutputStream(disks.get(2));
			fos.write(frag[2]);
			fos.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
