raidmiddleware
==============
Here are the deployment instructions to start and test the cloud-middleware.
To access our accounts on the cloud providers, you need following access data:

Accounts
========

Box Account:
------------
examine deployment_instructions.txt

Dropbox Account:
----------------
examine deployment_instructions.txt

Amazon S3 Account:
------------------
To use the Amazon S3 account within our solution the credentials file has to be 
moved to the folder %USERHOME%/.aws

The file "cloud.txt" in the folder ./raidmiddleware/raidmiddleware-ui stores the fetched authorization tokens for Box and Dropbox


Project Structure
=================

The project consists of the following modules:

- parent project (raidmiddleware): common metadata and configuration
- raidmiddleware-widgetset: widgetset, custom client side code and dependencies to widget add-ons
- raidmiddleware-ui: main application module, development time
- raidmiddleware-production: module that produces a production mode WAR for deployment


Workflow
========

Set up the MySQL database:
	- Install a MySQL database (We used XAMPP for installing the MySQL database)
	- Create a new database "raidmiddleware"
	- Use the given SQL-Skript "raidmiddleware.sql" to create the tables
	- Per default everything should be fine when using XAMPP
		- however there is the possibility to set another username or password in the file ./raidmiddleware/raidmiddleware-ui/src/main/java/db/MySQLConnection.java
		- change the lines String user = "root" and String pw = "" to the correct values
	- Start up the database

To compile the entire project, run "mvn install" in the "raidmiddleware" folder

To start the application run "mvn jetty:run" in ui module (Folder ./raidmiddleware/raidmiddleware-ui)
  - open http://localhost:8080/ with the web-browser
  
If everything worked you should see the user interface, where you can download/upload/delete files, create/delete folders and restore previous versions of files.
It is possible that the authoritation tokens of the cloud providers are expired (Box, Dropbox). In that case a new browser windows opens where you have to login
with the given accounts and authorize the application. For Dropbox you have to copy the authorization code into the command line and press enter, for Box it works
automatically.